-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 16 Jun 2019 pada 16.41
-- Versi Server: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_wp2`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id_admin`, `email`, `password`, `nama`) VALUES
(1, 'admin@admin.com', 'admin', 'administrator');

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_order`
--

CREATE TABLE `detail_order` (
  `id_order` varchar(200) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_order`
--

INSERT INTO `detail_order` (`id_order`, `id_produk`, `jumlah`) VALUES
('5ce6d61692e54', 2, 5),
('5ce6d61692e54', 1, 4),
('5ce6d61692e54', 9, 1),
('5ce6ea655027e', 9, 1),
('5ceaada716622', 13, 1),
('5ce6d61692e54', 2, 1),
('5ce6ea655027e', 2, 1),
('5ceb974858c2c', 2, 1),
('5cf0d1ffbcdef', 1, 2),
('5ceaada716622', 1, 6),
('5ceaada716622', 2, 1),
('5d03e8e6d25cf', 1, 6),
('5d03e8e6d25cf', 2, 1),
('5ce6d61692e54', 1, 2),
('5ce6d61692e54', 2, 1),
('5ce6ea655027e', 1, 2),
('5ce6ea655027e', 2, 1),
('5ceb974858c2c', 1, 2),
('5ceb974858c2c', 2, 1),
('5d03ee6157798', 1, 2),
('5d03ee6157798', 2, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori`
--

CREATE TABLE `kategori` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `nama_kategori`) VALUES
(1, 'Handphone'),
(3, 'Fashion'),
(4, 'laptop'),
(5, 'Sepatu');

-- --------------------------------------------------------

--
-- Struktur dari tabel `keranjang`
--

CREATE TABLE `keranjang` (
  `id_keranjang` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `orderan`
--

CREATE TABLE `orderan` (
  `id_order` varchar(200) NOT NULL,
  `id_user` int(11) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `tanggal_order` date NOT NULL,
  `status` int(11) NOT NULL COMMENT '0=batal,1=proses,2=dikirim,3=sukses'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `orderan`
--

INSERT INTO `orderan` (`id_order`, `id_user`, `alamat`, `tanggal_order`, `status`) VALUES
('5ce6d61692e54', 1, '', '2019-05-23', 3),
('5ce6ea655027e', 1, '', '2019-05-23', 3),
('5ceaada716622', 7, '', '2019-05-26', 1),
('5ceb974858c2c', 1, '', '2019-05-27', 1),
('5cf0d1ffbcdef', 12, 'jl.bojong rawalumbu blok c1/02 bekasi 18998', '2019-05-31', 3),
('5d03e8e6d25cf', 7, 'jl.bojong asri 3 blok c1/02 taman narogong indah bekasi 17226', '2019-06-14', 2),
('5d03ee6157798', 1, 'jl.bojong molek raya d19 no.2 perumahan taman narogong indah bekasi 17116', '2019-06-14', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `produk`
--

CREATE TABLE `produk` (
  `id_produk` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `harga` varchar(100) NOT NULL,
  `id_kategori` varchar(100) NOT NULL,
  `gambar` varchar(200) NOT NULL,
  `detail` varchar(100) NOT NULL,
  `stok` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `produk`
--

INSERT INTO `produk` (`id_produk`, `nama`, `harga`, `id_kategori`, `gambar`, `detail`, `stok`) VALUES
(1, 'Sepatu-Basket', '200000', '5', '12.jpg', 'ini sepatu buat main basket', '100'),
(2, 'tas', '150000', '3', '8.jpg', 'tas selempang', '50'),
(3, 'tas cewe', '299000', '3', '1.jpg', 'tas selempang cewe', '25'),
(9, 'Samsung s10++', '7000000', '1', 'gambar1557907508.jpg', 'ram 4GB initernal 64gb', '100'),
(12, 'Mac Pro 2019 ', '12000000', '4', 'gambar1557908128.jpg', ' x 256gb', '20'),
(13, 'Jam Rolex', '120000', '3', 'gambar1558641745.jpg', 'Jam tangan rolex 14\'mm automatic', '20');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `alamat` varchar(200) NOT NULL,
  `no_telp` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `nama`, `password`, `email`, `alamat`, `no_telp`) VALUES
(1, 'Faizz', '9d4d4ab0dfdb72a54b895d78b90b09c7', 'faiz@gmail.com', 'jl.bojong molek raya d19 no.2 perumahan taman narogong indah bekasi 17116', '08975128532'),
(2, 'john doe', 'b1734c3c466b3ddcdd3b841d02a24b56', 'user1@google.com', 'bekasi utara', '0988712632'),
(3, 'jane doe', '0d8d5cd06832b29560745fe4e1b941cf', 'user@gmail.com', 'jl.akasia block E3 Taman Raflesia Bekasi timur 16778', '0988776627'),
(5, 'bambang', 'a9711cbb2e3c2d5fc97a63e45bbe5076', 'bambang@gmail.com', 'pekayon utara rt01 no.3 17119', '098812371341'),
(6, 'zahra ', '01e50c681c0b05ff22686b3e0f7290d3', 'zahrafazalia18@gmail.com', 'jl.kecapi no.2 perumahan grand regency , mustikajaya , 17662', '0988123818'),
(7, 'faiz', '9d4d4ab0dfdb72a54b895d78b90b09c7', 'faizseeguns@gmail.com', 'jl.bojong asri 3 blok c1/02 taman narogong indah bekasi 17226', '102837812'),
(9, 'bayu aji kuncoro', '86381fd332169878703850e92e1241a9', 'badarvera20@gmail.com', 'jln bekasih timur regency citrine c12 n08 (17151)', '083871337705'),
(10, 'Muhammad Fachrurozi', 'e00fdc4e90d26fae3b1d83c1fa50707e', 'muhammadfchr19@gmail.com', 'Taman Tridaya Indah 1 F24 No3 TamSel', '0896356320424'),
(11, 'irsan ajie', '587adc422b4aa9d9ff1e4c2802a561d1', 'irsanajie1999@gmail.com', 'bekasi', '08138128772'),
(12, 'nyo', '03fbdb2363e43cf1af4971ad0db130dd', 'faiznyo12@gmail.com', 'jl.bojong indah 13 blok c1/02 bekasi 17226', '0897651263612');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `keranjang`
--
ALTER TABLE `keranjang`
  ADD PRIMARY KEY (`id_keranjang`);

--
-- Indexes for table `orderan`
--
ALTER TABLE `orderan`
  ADD PRIMARY KEY (`id_order`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id_produk`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `keranjang`
--
ALTER TABLE `keranjang`
  MODIFY `id_keranjang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id_produk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
