<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
    
    function __construct(){
        parent::__construct();
        
        // Load google oauth library
//        $this->load->library('google');
        
        // Load user model
//        $this->load->model('user');
    }
    
    function index(){
        redirect(base_url());
    }
   
}