<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	function __construct(){
		parent::__construct();
         $this->load->model('Email');
	}
    

	public function index(){
        $data['tittle']="Home";
        $data['items']=$this->Crud->get_data('produk')->result();
        $id_user=$this->session->userdata('id_user');
        $where_keranjang=array(
        'id_user'=>$id_user
        );
        $data['num_keranjang']=$this->Crud->edit_data($where_keranjang,'keranjang')->num_rows();
		$this->load->view('home',$data);
	}

	function login(){
        $data['tittle']="Login";
        $id_user=$this->session->userdata('id_user');
        $where_keranjang=array(
        'id_user'=>$id_user
        );
        $data['num_keranjang']=$this->Crud->edit_data($where_keranjang,'keranjang')->num_rows();
        $this->load->view('login',$data);
        
    }
      public function act_login()
    {
        $username = $this->input->post('email');
		$password = $this->input->post('pass');
		$this->form_validation->set_rules('email','Email','trim|required');
		$this->form_validation->set_rules('pass','Password','trim|required');
        
        if($this->form_validation->run() != false)
        {
            $where = array('email' => $username, 'password' => md5($password) );
			$data = $this->Crud->edit_data($where, 'user');
			$d = $this->Crud->edit_data($where, 'user')->row();
			$cek = $data->num_rows();

			if($cek > 0){
				$session = array(
                    'id_user' => $d->id_user,
                    'nama' => $d->nama,
                    'email' => $d->email,
                    'alamat' => $d->alamat,
                    'no_telp' => $d->no_telp,
                    'status' => 'login_user');
				$this->session->set_userdata($session);
				redirect(base_url());
            
               
			}else{
				$this->session->set_flashdata('alert', 'gagal');
				redirect(base_url().'home/login');
			}
         }
         
          
    }
    
    
    public function login_admin()
    {
        $data['tittle']="Login | Admin";
        $this->load->view('admin/login',$data);
    }
    
    public function act_login_admin()
    {
        $username = $this->input->post('admin_email');
		$password = $this->input->post('admin_password');
		$this->form_validation->set_rules('admin_email','Email','trim|required');
		$this->form_validation->set_rules('admin_password','Password','trim|required');
        
        if($this->form_validation->run() != false)
        {
            $where = array('email' => $username, 'password' => $password );
			$data = $this->Crud->edit_data($where, 'admin');
			$d = $this->Crud->edit_data($where, 'admin')->row();
			$cek = $data->num_rows();

			if($cek > 0){
				$session = array(
                    'id_admin' => $d->id_admin,
                    'nama' => $d->nama,
                    'email' => $d->email,
                    'status' => 'login_admin');
				$this->session->set_userdata($session);
				redirect(base_url().'admin');
                
			}else{
				$this->session->set_flashdata('alert', 'gagal');
				redirect(base_url().'home/login_admin');
			}
         }
    }
    
    public function register(){
        $nama=$this->input->post('nama');
        $email=$this->input->post('email');
        $password=md5($this->input->post('password'));
        $alamat=$this->input->post('alamat');
        $no_telp=$this->input->post('no_telp');
        
        $data=array(
            'nama'=>$nama,
            'email'=>$email,
            'password'=>$password,
            'alamat'=>$alamat,
            'no_telp'=>$no_telp
            
        );
        $cek = $this->Crud->edit_data(array('email'=>$email),'user')->num_rows();
        if ($cek>0){
             $this->session->set_flashdata('alert', '<b style="color:red;">email sudah terdaftar !</b>');
             redirect(base_url().'home/login');
        }else{
            $this->session->set_flashdata('alert', '<b style="color:blue;">Sukses , Silahkan Login </b>');
             $this->Crud->insert_data($data,'user'); 
            $this->Email->send($nama,$email,'Selamat anda sukses Mendaftar Di KiosKu');
             redirect(base_url().'home/login');
        }
        
    }
    
    function subscribe(){
        $email=$this->input->post('email');
        $this->Email->send($email,$email,'Selamat anda Berhasil Berlanganan Di KiosKu');
        redirect(base_url());
    }

    
}

		

