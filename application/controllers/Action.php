<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Action extends CI_Controller {
	function __construct(){
		parent::__construct();
        if($this->session->userdata('status') != "login_admin"){
			$alert=$this->session->set_flashdata('alert', 'belum login');
			redirect(base_url().'home/login_admin');
		}
		  $this->load->helper(array('form', 'url'));
	
	}

	function add_user(){
        $nama=$this->input->post('nama');
        $email=$this->input->post('email');
        $password=md5($this->input->post('password'));
        $alamat=$this->input->post('alamat');
        $no_telp=$this->input->post('no_telp');
        
        $data=array(
            'nama'=>$nama,
            'email'=>$email,
            'password'=>$password,
            'alamat'=>$alamat,
            'no_telp'=>$no_telp
            
        );
        $cek = $this->Crud->edit_data(array('email'=>$email),'user')->num_rows();
        if ($cek>0){
             $this->session->set_flashdata('alert', 'email terdaftar');
             redirect(base_url().'admin/user_tambah');
        }else{
          
             $this->Crud->insert_data($data,'user');
            redirect(base_url().'admin/user_member');
        }
        
    }
    function delete_user($id){
        $where=array(
            'id_user'=>$id
        );
        $this->Crud->delete_data($where,'user');
        redirect(base_url().'admin/user_member');
        
    }
    function edit_user($id=''){
        $nama=$this->input->post('nama');
        $email=$this->input->post('email');
        $password=md5($this->input->post('password'));
        $alamat=$this->input->post('alamat');
        $no_telp=$this->input->post('no_telp');
        
        $data=array(
            'nama'=>$nama,
            'email'=>$email,
            'password'=>$password,
            'alamat'=>$alamat,
            'no_telp'=>$no_telp
            
        );
        $data2=array(
            'nama'=>$nama,
            'email'=>$email,
            'alamat'=>$alamat,
            'no_telp'=>$no_telp
            
        );
        
        $where=array(
            'id_user'=>$id
        );
        if(empty($password)){
            $this->Crud->update_data($where,$data2,'user');
        }else{
            $this->Crud->update_data($where,$data,'user');
        }
        
        $this->session->set_flashdata('alert', 'sukses');
        redirect(base_url().'admin/user_member');
    }
    
    function edit_admin(){
        $pass=$this->input->post('pass');
        $data=array(
        'password'=>$pass
        );
        $this->Crud->update_data($where=array('id_admin'=>'1'),$data,'admin');
        $this->session->set_flashdata('alert', 'sukses');
        redirect(base_url().'admin/user_admin');
    }
    
    
    function delete_produk($id){
        $where=array(
            'id_produk'=>$id
        );
        $this->Crud->delete_data($where,'produk');
        redirect(base_url().'admin/produk');
        
    }
    
    function add_produk(){
        $nama=$this->input->post('nama');
        $harga=$this->input->post('harga');
        $stok=$this->input->post('stok');
        $detail=$this->input->post('detail');
        $kategori=$this->input->post('kategori');
        
        
       
        //configurasi upload gambar
		$config['upload_path']          = './assets_frontend/img/products';
		$config['allowed_types']        = '*';
		$config['max_size']             = '2048';
        $config['file_name']            = 'gambar'.time();
		$config['max_width']            = 1500;
		$config['max_height']           = 1000;

		$this->load->library('upload', $config);
        $ext=pathinfo($_FILES["berkas"]["name"],PATHINFO_EXTENSION);
        $gambar =  $config['file_name'].'.'.$ext;
            
        
         $data_produk=array(
            'nama'=>$nama,
            'harga'=>$harga,
            'stok'=>$stok,
            'detail'=>$detail,
            'gambar'=>$gambar ,
            'id_kategori'=>$kategori
            
        );
        

		if ( ! $this->upload->do_upload('berkas')){
			$error= $this->upload->display_errors();
			echo $error;
            $base_url=base_url().'admin';
            echo "<a href='$base_url'>Back</a>";
		}else{
			$data = array('upload_data' => $this->upload->data());
			 $this->Crud->insert_data($data_produk,'produk');
             $this->session->set_flashdata('alert', 'Sukses');
             redirect(base_url().'admin/produk');
            
		}
            
    }
    
    function edit_produk($id){
        $nama=$this->input->post('nama');
        $harga=$this->input->post('harga');
        $stok=$this->input->post('stok');
        $detail=$this->input->post('detail');
        $kategori=$this->input->post('kategori');
        $where=array(
        'id_produk'=>$id
        );
        //configurasi upload gambar
		$config['upload_path']          = './assets_frontend/img/products';
		$config['allowed_types']        = 'gif|jpg|png';
		$config['max_size']             = '2048';
        $config['file_name']            = 'gambar'.time();
		$config['max_width']            = 4096;
		$config['max_height']           = 3000;

		$this->load->library('upload', $config);
        $ext=pathinfo($_FILES["berkas"]["name"],PATHINFO_EXTENSION);
        $gambar =  $config['file_name'].'.'.$ext;
        $img=$this->input->post('gambar');    
        
         $data_produk=array(
            'nama'=>$nama,
            'harga'=>$harga,
            'stok'=>$stok,
            'detail'=>$detail,
            'gambar'=>$gambar ,
            'id_kategori'=>$kategori
            
        );
        $data_produk_img=array(
            'nama'=>$nama,
            'harga'=>$harga,
            'stok'=>$stok,
            'detail'=>$detail,
            'gambar'=>$img ,
            'id_kategori'=>$kategori
            
        );
        
                
        
        if ( ! $this->upload->do_upload('berkas')){
			$this->Crud->update_data($where,$data_produk_img,'produk');
             $this->session->set_flashdata('alert', 'Sukses');
             redirect(base_url().'admin/produk');
		}else{
			$data = array('upload_data' => $this->upload->data());
			 $this->Crud->update_data($where,$data_produk,'produk');
             $this->session->set_flashdata('alert', 'Sukses');
             redirect(base_url().'admin/produk');
            
		}
      
    }
    
    function delete_kategori($id){
        $where=array(
            'id_kategori'=>$id
        );
        $this->Crud->delete_data($where,'kategori');
        redirect(base_url().'admin/kategori');
        
    }
    
    function add_kategori(){
        $kategori=$this->input->post('kategori');
        $this->Crud->insert_data(array('nama_kategori'=>$kategori),'kategori');
        redirect(base_url().'admin/kategori');
        
    }
    
    function edit_kategori($id){
        $kategori=$this->input->post('kategori');
        $where=array(
            'id_kategori'=>$id
        );
        $this->Crud->update_data($where,array('nama_kategori'=>$kategori),'kategori');
        redirect(base_url().'admin/kategori');
        
    }
    
    function edit_transaksi(){
     $id=$this->input->post('id_order');
     $status=$this->input->post('status');
     $where=array('id_order'=>$id);
     $this->Crud->update_data($where,array('status'=>$status),'orderan');
        
        redirect(base_url().'admin/transaksi');
        
        
    }
}/*end*/

		

