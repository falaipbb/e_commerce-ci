<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Controller {
	function __construct(){
		parent::__construct();
        
	}
    

	public function detail($id){
        
        $data['tittle']="Detail | Produk";
        $where=array('id_produk'=>$id);
        $data['item']=$this->db->query("Select * from produk,kategori where produk.id_produk='$id' and kategori.id_kategori=produk.id_kategori  ")->row();
        $id_user=$this->session->userdata('id_user');
        $where_keranjang=array(
        'id_user'=>$id_user
        );
        $data['num_keranjang']=$this->Crud->edit_data($where_keranjang,'keranjang')->num_rows();
		$this->load->view('detail',$data);
	}
	public function katalog(){
        $by=$this->input->get('by');
        
        $where=array(
        'id_kategori'=>$by
        );
        
        
        if (empty($by)){
            $data['items']=$this->Crud->get_data('produk')->result();
        }else{
            $data['items']=$this->Crud->edit_data($where,'produk')->result();
        }
        
        
        $data['kategori']=$this->Crud->get_data('kategori')->result();
		$data['tittle']="katalog | Produk";
		$id_user=$this->session->userdata('id_user');
        $where_keranjang=array(
        'id_user'=>$id_user
        );
        $data['num_keranjang']=$this->Crud->edit_data($where_keranjang,'keranjang')->num_rows();
		$this->load->view('katalog',$data);

	}
	public function tambah_katalog(){
        // persiapan variable
        $id_produk=$this->input->post('id_produk');
		$jumlah=$this->input->post('jumlah');
        $id=$this->session->userdata('id_user');
		$data= array(
			'id_produk'=>$id_produk,
			'jumlah'=>$jumlah,
            'id_user'=>$id
		);
        
        $where=array('id_produk'=>$id_produk,'id_user'=>$id);
        $cek=$this->Crud->edit_data($where,'keranjang')->num_rows();
        $prepare=$this->Crud->edit_data($where,'keranjang')->row();
        $add=$prepare->jumlah+$jumlah;
        $data_keranjang=array(
        'jumlah'=>$add
        );
        //exsekusi :D
        
         if($this->session->userdata('status') != "login_user"){
			$alert=$this->session->set_flashdata('alert', 'belum login');
			redirect(base_url().'home/login');
		}else{
            if($cek>0){
                
                $this->Crud->update_data($where,$data_keranjang,'keranjang');
                redirect(base_url().'produk/katalog');
                
            }else{
                $this->Crud->insert_data($data,'keranjang');
		        redirect(base_url().'produk/katalog');
            } 
		 }
	}

}