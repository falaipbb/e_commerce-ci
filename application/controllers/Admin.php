<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	function __construct(){
		parent::__construct();
        if($this->session->userdata('status') != "login_admin"){
			$alert=$this->session->set_flashdata('alert', 'belum login');
			redirect(base_url().'home/login_admin');
		}
	}

	public function index(){
        $data['tittle']="Dashboard";
        $data['num_user']=$this->Crud->get_data('user')->num_rows();
        $data['num_produk']=$this->Crud->get_data('produk')->num_rows();
        $data['keuntungan']=$this->db->query('SELECT * FROM produk,
            detail_order,orderan WHERE produk.id_produk=detail_order.id_produk And orderan.status=3 and orderan.id_order=detail_order.id_order ')->result();
		$this->load->view('admin/dashboard',$data);
	}
    
    public function logout(){
        $this->session->sess_destroy();
		redirect(base_url().'home/login_admin');
    }
    
    function user_admin(){
        $data['tittle']="User | Admin";
		$this->load->view('admin/user_admin',$data);

    }
    
     function user_member(){
        $data['user']=$this->Crud->get_data('user')->result();
        $data['tittle']="User | member";
		$this->load->view('admin/user_member',$data);

    }
    
    function user_edit($id){
               
        $where = array ('id_user'=>$id);
        $data['user']=$this->Crud->edit_data($where,'user')->row();
                
        $data['tittle']="User | edit";
        $this->load->view('admin/user_edit',$data);            
                 
                
    }
    
    function user_tambah(){
        $data['tittle']="User | tambah";
        $this->load->view('admin/user_tambah',$data);  
    }
    
    function produk(){
        $data['tittle']="Data | Produk";
        $data['produk']=$this->Crud->get_data('produk')->result();
        $data['kategori']=$this->Crud->get_data('kategori')->result();
        $this->load->view('admin/produk',$data);
    }
    function produk_tambah(){
         $data['tittle']="Tambah | Produk";      
        $data['kategori']=$this->Crud->get_data('kategori')->result();
        $this->load->view('admin/produk_tambah',$data);
    }
    function produk_edit($id){
        $data['all_kategori']=$this->Crud->get_data('kategori')->result();
        $data['tittle']="Produk | edit";

        $query="SELECT * from Produk P, Kategori K WHERE P.id_kategori=P.id_Kategori and P.id_produk=$id ";
        $data['item'] = $this->db->query($query)->result();
        foreach($data['item'] as $i){
            $data['produk']=$i;
        }
        
        $this->load->view('admin/produk_edit',$data);   
    }
    
    function kategori(){
        $data['tittle']="Data | Kategori";
        $data['produk']=$this->Crud->get_data('produk')->result();
        $data['kategori']=$this->Crud->get_data('kategori')->result();
        $this->load->view('admin/kategori',$data);
        
    }
    function kategori_tambah(){
        $data['tittle']="Data | Kategori";
        $data['produk']=$this->Crud->get_data('produk')->result();
        $data['kategori']=$this->Crud->get_data('kategori')->result();
        $this->load->view('admin/kategori_tambah',$data);
        
    }
    function kategori_edit($id){
        $data['tittle']="Data | Kategori";
        $where=array(
            'id_kategori'=>$id
        );
        $data['produk']=$this->Crud->get_data('produk')->result();
        $data['kategori']=$this->Crud->edit_data($where,'kategori')->row();
        $this->load->view('admin/kategori_edit',$data);
        
    }
    
    function transaksi(){
         $data['tittle']="Data | transaksi";
        $query="Select  * from orderan,user where orderan.id_user=user.id_user";
        $data['order']=$this->db->query($query)->result();
        $this->load->view('admin/transaksi',$data);
    }
    
    function transaksi_edit($id){
         $data['tittle']="Edit | transaksi";
        $query="Select  * from orderan,detail_order,user where orderan.id_user=user.id_user and detail_order.id_order=orderan.id_order And orderan.id_order='$id'";
       $data['items']=$this->db->query($query)->result();
        $data['produk']=$this->db->query("Select * from produk,detail_order where detail_order.id_order='$id' and produk.id_produk=detail_order.id_produk")->result();
        foreach ($data['items'] as $h){
             $data['order']=$h;
        }
        $this->load->view('admin/transaksi_edit',$data);
//        print_r($data['order']);
    }
    
    function laporan_produk(){
        $data['tittle']="Data | Produk";
        $data['produk']=$this->Crud->get_data('produk')->result();
        $data['kategori']=$this->Crud->get_data('kategori')->result();
        $this->load->view('admin/laporan_produk',$data);
    }
    
    function laporan_user(){
        $data['user']=$this->Crud->get_data('user')->result();
        $data['tittle']="User | member";
		$this->load->view('admin/laporan_user',$data);

    }
    
    function laporan_transaksi(){
         $data['tittle']="Data | transaksi";
        $query="Select  * from orderan,user where orderan.id_user=user.id_user";
        $data['order']=$this->db->query($query)->result();
        $this->load->view('admin/laporan_transaksi',$data);
    }
    
    function laporan_pdf_produk(){
        
        $data['produk'] = $this->Crud->get_data('produk')->result();
        $this->load->view('admin/pdf_produk', $data);
        
        $html = $this->output->get_output();

        $this->load->library('dompdf_gen');
        
        $paper_size = 'A4'; //ukuran kertas
        $orientation = 'landscape';
                
        $this->dompdf->set_paper($paper_size, $orientation);
        
        //convert to pdf
        $this->dompdf->load_html($html);
        $this->dompdf->render();
        $this->dompdf->stream("laporan_data_produk.pdf", array('Attachment'=>0));
        //nama file yang dihasilkan
    }
    
    function laporan_pdf_user(){
        
        $data['user'] = $this->Crud->get_data('user')->result();
        $this->load->view('admin/pdf_user', $data);
        
        $html = $this->output->get_output();

        $this->load->library('dompdf_gen');
        
        $paper_size = 'A4'; //ukuran kertas
        $orientation = 'landscape';
                
        $this->dompdf->set_paper($paper_size, $orientation);
        
        //convert to pdf
        $this->dompdf->load_html($html);
        $this->dompdf->render();
        $this->dompdf->stream("laporan_data_user.pdf", array('Attachment'=>0));
        //nama file yang dihasilkan
    }
    
}

		

