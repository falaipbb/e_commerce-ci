<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	function __construct(){
		parent::__construct();
        if($this->session->userdata('status') != "login_user"){
			$alert=$this->session->set_flashdata('alert', 'belum login');
			redirect(base_url().'home/login');
		}
	}
    function index(){
         $data['tittle']="User |info";
        $id_user=$this->session->userdata('id_user');
         $where_keranjang=array(
        'id_user'=>$id_user
        );
        $data['user']=$this->Crud->edit_data(array('id_user'=>$id_user),'user')->row();
        $data['item']=$this->Crud->edit_data(array('id_user'=>$id_user),'orderan')->result();
        $data['num_keranjang']=$this->Crud->edit_data($where_keranjang,'keranjang')->num_rows();
        
        $this->load->view('user',$data);
    }
    function detail($id_order=''){
        $id_user=$this->session->userdata('id_user');
         $data['tittle']="User |detail";
          $where_keranjang=array(
        'id_user'=>$id_user
        );
        $data['orderan']=$this->Crud->edit_data(array('id_user'=>$id_user,'id_order'=>$id_order),'orderan')->row();
         $data['item']=$this->db->query("SELECT * FROM produk,
            detail_order WHERE produk.id_produk=detail_order.id_produk AND detail_order.id_order='$id_order' ")->result();
        $data['num_keranjang']=$this->Crud->edit_data($where_keranjang,'keranjang')->num_rows();
         $this->load->view('detail_order',$data);
    }
    
    function edit($id_user=''){
        $nama=$this->input->post('nama');
        $email=$this->input->post('email');
        $password=md5($this->input->post('password'));
        $alamat=$this->input->post('alamat');
        $no_telp=$this->input->post('no_telp');
        $pw=$this->input->post('password');
        $data=array(
            'nama'=>$nama,
            'email'=>$email,
            'password'=>$password,
            'alamat'=>$alamat,
            'no_telp'=>$no_telp
            
        );
        $data2=array(
            'nama'=>$nama,
            'email'=>$email,
            'alamat'=>$alamat,
            'no_telp'=>$no_telp
            
        );
        
        $where=array(
            'id_user'=>$id_user
        );
        
        
        if(!empty($pw)){
            $this->Crud->update_data($where,$data,'user');
        }
        else {
             $this->Crud->update_data($where,$data2,'user');
        }
        
         
        $this->session->set_flashdata('alert', 'sukses');
        redirect(base_url().'user');
    }
    
    function logout(){
         $this->session->sess_destroy();
        redirect(base_url());
    }
    function cart(){
        $data['tittle']="Cart";
        $id_user=$this->session->userdata('id_user');
        $where_keranjang=array(
        'id_user'=>$id_user
        );
        $data['item']=$this->db->query("SELECT * FROM produk,
            keranjang WHERE produk.id_produk=keranjang.id_produk AND keranjang.id_user=$id_user ")->result();
        
        $data['num_keranjang']=$this->Crud->edit_data($where_keranjang,'keranjang')->num_rows();
        $this->load->view('cart',$data);
    }
    
    function delete_cart($id){
        $where=array('id_keranjang' => $id);
        $this->Crud->delete_data($where,'keranjang');
        redirect(base_url().'user/cart');
    }
    
    function checkout(){
         $id_user=$this->session->userdata('id_user');
       $data['item']=$this->db->query("SELECT * FROM produk,
            keranjang WHERE produk.id_produk=keranjang.id_produk AND keranjang.id_user=$id_user ")->result();
      
       $data['user']=$this->Crud->edit_data(array('id_user'=>$id_user),'user')->row();    
       $data['tittle']="Checkout";
       $where_keranjang=array(
        'id_user'=>$id_user
        );
        $data['num_keranjang']=$this->Crud->edit_data($where_keranjang,'keranjang')->num_rows();
        if($data['num_keranjang']>0){
            $this->load->view('checkout',$data);
        }else{
            redirect(base_url().'user/cart');
        }
        
        
        
    }
    
    function order($id_order=''){
        $id_user=$this->session->userdata('id_user');
        $alamat=$this->input->post('alamat');
        $data=array(
        'id_order'=>$id_order,
        'id_user'=>$id_user,
        'tanggal_order'=>date('Y-m-d'),
        'alamat'=>$alamat,
        'status'=>1
        );
        
        $this->Crud->insert_data($data,'orderan');
        $this->Crud->insert_detail($id_user);
        $this->Crud->delete_data(array('id_user'=>$id_user),'keranjang');
        redirect(base_url().'user');
    }
    
}

