
    <!-- jquery 3.12.4 -->
    <script src="<?php echo base_url();?>assets_frontend/js/vendor/jquery-1.12.4.min.js"></script>
    <!-- mobile menu js  -->
    <script src="<?php echo base_url();?>assets_frontend/js/jquery.meanmenu.min.js"></script>
    <!-- scroll-up js -->
    <script src="<?php echo base_url();?>assets_frontend/js/jquery.scrollUp.js"></script>
    <!-- owl-carousel js -->
    <script src="<?php echo base_url();?>assets_frontend/js/owl.carousel.min.js"></script>
    <!-- slick js -->
    <script src="<?php echo base_url();?>assets_frontend/js/slick.min.js"></script>
    <!-- wow js -->
    <script src="<?php echo base_url();?>assets_frontend/js/wow.min.js"></script>
    <!-- price slider js -->
    <script src="<?php echo base_url();?>assets_frontend/js/jquery-ui.min.js"></script>
    <script src="<?php echo base_url();?>assets_frontend/js/jquery.countdown.min.js"></script>
    <!-- nivo slider js -->
    <script src="<?php echo base_url();?>assets_frontend/js/jquery.nivo.slider.js"></script>
    <!-- fancybox js -->
    <script src="<?php echo base_url();?>assets_frontend/js/jquery.fancybox.min.js"></script>
    <!-- bootstrap -->
    <script src="<?php echo base_url();?>assets_frontend/js/bootstrap.min.js"></script>
    <!-- stellar -->
    <script src="<?php echo base_url();?>assets_frontend/js/jquery.stellar.min.js"></script>
    <script src="<?php echo base_url();?>assets_frontend/js/parallax.js"></script>
    <!-- popper -->
    <script src="<?php echo base_url();?>assets_frontend/js/popper.js"></script>
    <!-- plugins -->
    <script src="<?php echo base_url();?>assets_frontend/js/plugins.js"></script>
    <!-- main js -->
    <script src="<?php echo base_url();?>assets_frontend/js/main.js"></script>
    <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5cf0e39b267b2e5785304b3e/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->