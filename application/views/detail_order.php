<!doctype html>
<?php
 if($orderan->status==1){
    $status='Diproses';
  }else if($orderan->status==2){
    $status='Dikirim';
  }else if($orderan->status==3){
    $status='Sukses'; 
  }else{
    $status='Dibatalkan'; 
   }
?>
<html class="no-js" lang="zxx">

<!--include header-->
<?php $this->load->view('header'); ?>

<body>
    <!-- Wrapper Start -->
    <div class="wrapper kiosk-home">
        <!-- Header Area Start -->
        <?php $this->load->view('nav'); ?>

       
        <div class="kiosk-products-cart ptb-70 pb-sm-50">
            <div class="container">
               <h2 class="text-center">Detail Order </h2>
               <hr>
              
                <div class="row">
                   <div class="col-md-4">
                        <div class="main-right-sidebar">
                            <!-- Recent Post Start -->
                            <div class="recent-post same-sidebar border-default">
                                <h3 class="sidebar-title">Status</h3>
                                <ul>
                                    <!-- Singel Post Thumb Start -->
                                    <li class="post-thumb fix">
                                        <div class="left-post-thumb f-left mr-20 mb-20">
                                            <marquee ><b><?php echo $status; ?></b></marquee>
                                            
                                        </div>
                                        <div class="right-post-thumb fix">
                                            <h4><a href="#">No order #<?php echo $this->uri->segment('3'); ?></a></h4>
                                            <span><b><?php echo $orderan->tanggal_order; ?></b></span>
                                           
                                            <span><?php echo $orderan->alamat; ?></span>
                                            
                                        </div>
                                    </li>
                                 
                                </ul>
                            </div>
                           
                        </div>
                       
                   </div>
                    <div class="col-md-8">
                        <!-- Form Start -->
                        <form action="#">
                            <!-- Table Content Start -->
                            <div class="table-content table-responsive">
                                <table>
                                    <thead>
                                        <tr>
                                            <th class="kiosk-product-thumb">Image</th>
                                            <th class="product-name">Nama</th>
                                            <th class="product-price">Harga</th>
                                            <th class="product-quantity">Jumlah</th>
                                            <th class="product-total">Total</th>
                                           
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $total_harga=0;
                                        $total_quantity=0;
                                        
                                        
                                        foreach ($item as $i) { 
                                        
                                        ?>
                                        <tr>

                                            <td class="kiosk-product-thumb">
                                                <a href="#"><img src="<?php echo base_url()."assets_frontend/img/products/".$i->gambar;?>" alt="cart-image" /></a>
                                            </td>
                                            <td class="product-name"><a href="#">
                                                    <?php echo $i->nama;?></a></td>
                                            <td class="product-price"><span class="amount">
                                                    <?php echo $i->harga;?></span></td>
                                            <td class="product-quantity">
                                                <input readonly value="<?php echo $i->jumlah;?>" type="disable">
                                            </td>
                                            <td class="product-total text-center"><span class="amount">
                                                    <?php echo number_format($i->harga*$i->jumlah,0,',','.'); ?></span></td>
                                            
                                        </tr>
                                        <?php 
                                        $total_quantity += $i->jumlah;
                                        $total_harga += ($i->harga*$i->jumlah);  
                                        } ?>
                                        <td colspan="4">TOTAL</td>
                                        <td><b style="font-size:20px;">Rp.<?php echo number_format($total_harga,0,',','.'); ; ?></b></td>
                                    </tbody>
                                </table>
                            </div>
                            <div class="pull-left kiosk-cart-button">

                                <a href="<?php echo base_url().'user';?>" class="button slider-btn f-right mr-0">Back</a>

                            </div>
                            <div class="pull-right kiosk-cart-button">


                            </div>
                            <!-- Table Content Start -->
                        </form>

                        <!-- Form End -->
                    </div>
                </div>
                <!-- Row End -->
            </div>
        </div>
       
    </div>
    <!-- Wrapper End -->


    <!--include footer    -->
    <?php $this->load->view('footer'); ?>
</body>


</html>