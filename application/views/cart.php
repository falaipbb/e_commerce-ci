<!doctype html>
<html class="no-js" lang="zxx">

<!--include header-->
<?php $this->load->view('header'); ?>

<body>
    <!-- Wrapper Start -->
    <div class="wrapper kiosk-home">
        <!-- Header Area Start -->
        <?php $this->load->view('nav'); ?>
         <?php $this->load->view('bg'); ?>
        <?php if($num_keranjang>0){ ?>
        <div class="kiosk-products-cart ptb-70 pb-sm-50">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Form Start -->
                        <form action="#">
                            <!-- Table Content Start -->
                            <div class="table-content table-responsive">
                                <table>
                                    <thead>
                                        <tr>
                                            <th class="kiosk-product-thumb">Image</th>
                                            <th class="product-name">Nama</th>
                                            <th class="product-price">Harga</th>
                                            <th class="product-quantity">Jumlah</th>
                                            <th class="product-total">Total</th>
                                            <th class="kiosk-product-remove">Hapus</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($item as $i) { ?>
                                        <tr>

                                            <td class="kiosk-product-thumb">
                                                <a href="#"><img src="<?php echo base_url()."assets_frontend/img/products/".$i->gambar;?>" alt="cart-image" /></a>
                                            </td>
                                            <td class="product-name"><a href="#">
                                                    <?php echo $i->nama;?></a></td>
                                            <td class="product-price"><span class="amount">
                                                    <?php echo $i->harga;?></span></td>
                                            <td class="product-quantity">
                                                <input readonly value="<?php echo $i->jumlah;?>" type="disable">
                                            </td>
                                            <td class="product-total text-center"><span class="amount">
                                                    <?php echo $i->harga*$i->jumlah;?></span></td>
                                            <td class="product-add-to-cart"><a href="<?php echo base_url();?>User/delete_cart/<?php echo $i->id_keranjang;?>">X</a></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="pull-left kiosk-cart-button">

                                <a href="<?php echo base_url();?>produk/katalog" class="button slider-btn f-right mr-0">Back</a>

                            </div>
                            <div class="pull-right kiosk-cart-button">

                                <a href="<?php echo base_url();?>user/checkout" class="button slider-btn f-right mr-0">Checkout</a>

                            </div>
                            <!-- Table Content Start -->
                        </form>

                        <!-- Form End -->
                    </div>
                </div>
                <!-- Row End -->
            </div>
        </div>
        <?php }else{ ?>

        <div class="section-title col-lg-12 mt-100 alert-message text-center">
            <h2>
                Upsss! Keranjang Kosong, silahkan belanja :D <i class="fa fa-warning"></i></h2>
            <div class="kiosk-cart-button ">
                <a class="button slider-btn" href="<?php echo base_url().'produk/katalog' ?>">Lihat Katalog Produk</a>
            </div>
        </div>

        <?php } ?>
    </div>
    <!-- Wrapper End -->


    <!--include footer    -->
    <?php $this->load->view('footer'); ?>
</body>


</html>