<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
<!--include style-->
<?php $this->load->view('admin/style');  ?>
<!--end include style-->
</head>

<body class="">
 
  <div class="wrapper ">
<!--  admin/side bar include-->
<?php $this->load->view('admin/sidebar'); ?>
<!--end side bar   -->
        <div class="main-panel">
<!-- Navbar -->
<?php $this->load->view('admin/navbar'); ?>
<!-- End Navbar -->
<!--     -->
<!--konten di sini     -->
<div class="content">
     <div class="content">
             <div class="container-fluid">
          <div class="row">
            <div class="col-md-8 ml-auto mr-auto">
              <div class="card ">
                <div class="card-header card-header-rose card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">mail_outline</i>
                  </div>
                  <h4 class="card-title">Edit Data User</h4>
                </div>
                <div class="card-body ">
                  <form method="post" action="<?php echo base_url().'action/edit_user/'.$user->id_user; ?>">
                    <div class="form-group">
                      <label for="exampleEmail" class="bmd-label-floating">Email Address</label>
                      <input type="email" class="form-control"  value="<?php echo $user->email; ?>"name="email" id="exampleEmail">
                    </div>
                    <div class="form-group">
                      <label for="exampleEmail" class="bmd-label-floating">Nama</label>
                      <input type="text" class="form-control"  value="<?php echo $user->nama; ?>"name="nama" id="exampleEmail">
                    </div>
                    <div class="form-group">
                      <label for="examplePass" class="bmd-label-floating">Password</label>
                      <input type="password" class="form-control"  placeholder="                    Isi untuk merubah password" name="password" id="examplePass">
                    </div>
                    <div class="form-group">
                      <label for="exampleEmail" class="bmd-label-floating">Alamat</label>
                      <input type="text" class="form-control"  value="<?php echo $user->alamat; ?>"name="alamat" id="exampleEmail">
                    </div>
                    <div class="form-group">
                      <label for="exampleEmail" class="bmd-label-floating">No telpon</label>
                      <input type="text" class="form-control"  value="<?php echo $user->no_telp; ?>"name="no_telp" id="exampleEmail">
                    </div>
                    <div class="card-footer ">
                  <button type="submit" class="btn btn-fill btn-success">Simpan</button>
                  
                  <a href="<?php echo base_url().'admin/user_member'; ?>">
                        <button type="button" class="btn btn-danger btn-fill" >Batal</button>
                  </a>
                </div>
                  </form>
                </div>
                
              </div>
              </div></div></div>
</div>     
</div>     
<!---->
<!---->
<!--include footer -->
<?php $this->load->view('admin/footer'); ?>   
<!--/footer-->
                 
        </div>
     </div>
              
              
            
              <!--   Core JS Files   -->
              <script src="<?php echo base_url() ?>admin_assets/js/core/jquery.min.js"></script>
              <script src="<?php echo base_url() ?>admin_assets/js/core/popper.min.js"></script>
              <script src="<?php echo base_url() ?>admin_assets/js/core/bootstrap-material-design.min.js"></script>
              <script src="<?php echo base_url() ?>admin_assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
              <!-- Plugin for the momentJs  -->
              <script src="<?php echo base_url() ?>admin_assets/js/plugins/moment.min.js"></script>
              <!--  Plugin for Sweet Alert -->
              <script src="<?php echo base_url() ?>admin_assets/js/plugins/sweetalert2.js"></script>
              <!-- Forms Validations Plugin -->
              <script src="<?php echo base_url() ?>admin_assets/js/plugins/jquery.validate.min.js"></script>
              <!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
              <script src="<?php echo base_url() ?>admin_assets/js/plugins/jquery.bootstrap-wizard.js"></script>
              <!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
              <script src="<?php echo base_url() ?>admin_assets/js/plugins/bootstrap-selectpicker.js"></script>
              <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
              <script src="<?php echo base_url() ?>admin_assets/js/plugins/bootstrap-datetimepicker.min.js"></script>
              <!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
              <script src="<?php echo base_url() ?>admin_assets/js/plugins/jquery.dataTables.min.js"></script>
              <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
              <script src="<?php echo base_url() ?>admin_assets/js/plugins/bootstrap-tagsinput.js"></script>
              <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
              <script src="<?php echo base_url() ?>admin_assets/js/plugins/jasny-bootstrap.min.js"></script>
              <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
              <script src="<?php echo base_url() ?>admin_assets/js/plugins/fullcalendar.min.js"></script>
              <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
              <script src="<?php echo base_url() ?>admin_assets/js/plugins/jquery-jvectormap.js"></script>
              <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
              <script src="<?php echo base_url() ?>admin_assets/js/plugins/nouislider.min.js"></script>
              <!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
              <script src="<?php echo base_url() ?>admin_assets/cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
              <!-- Library for adding dinamically elements -->
              <script src="<?php echo base_url() ?>admin_assets/js/plugins/arrive.min.js"></script>
              <!--  Google Maps Plugin    -->
              <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2Yno10-YTnLjjn_Vtk0V8cdcY5lC4plU"></script>
              <!-- Place this tag in your head or just before your close body tag. -->
              <script async defer src="<?php echo base_url() ?>admin_assets/buttons.github.io/buttons.js"></script>
              <!-- Chartist JS -->
              <script src="<?php echo base_url() ?>admin_assets/js/plugins/chartist.min.js"></script>
              <!--  Notifications Plugin    -->
              <script src="<?php echo base_url() ?>admin_assets/js/plugins/bootstrap-notify.js"></script>
              <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
              <script src="<?php echo base_url() ?>admin_assets/js/material-dashboard.minf066.js?v=2.1.0" type="text/javascript"></script>
             
              <!-- Sharrre libray -->
              <script src="<?php echo base_url() ?>admin_assets/demo/jquery.sharrre.js"></script>
             
</body>


</html>