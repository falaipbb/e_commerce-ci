<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
<!--include style-->
<?php $this->load->view('admin/style');  ?>
<!--end include style-->
</head>

<body class="">
 
  <div class="wrapper ">
<!--  admin/side bar include-->
   <?php $this->load->view('admin/sidebar'); ?>
<!--end side bar   -->
        <div class="main-panel">
<!-- Navbar -->
<?php $this->load->view('admin/navbar'); ?>
<!-- End Navbar -->
<!--     -->
<!--konten di sini     -->
<div class="content">
        <div class="container-fluid">
         <?php if($this->session->flashdata('alert')=='sukses'){ ?>
      <center> 
                   <div class="alert alert-success alert-message" style="max-width:400px; ">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <i class="material-icons">close</i>
                    </button>
                    <span>
                      <b> Sukses - </b>  Data Berhasil di edit</span>
                  </div>
      </center>
      <?php } ?>
            <div class="row">
            <div class="col-md-8 ml-auto mr-auto">
              <div class="card ">
                <div class="card-header card-header-rose card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">mail_outline</i>
                  </div>
                  <h4 class="card-title">Edit Transaksi (<?php echo $order->id_order; ?>)</h4>
                </div>
                <div class="card-body ">
                 

                 
                    <?php echo form_open_multipart(base_url().'action/edit_transaksi/'.$order->id_order)?>
                    
                     <div class="form-group">
                      <label for="exampleEmail" class="bmd-label-floating">ID Order</label>
                      <input disabled type="text" class="form-control"    value="<?php echo $order->id_order; ?>">
                    </div>
                    <div class="form-group">
                      <label for="exampleEmail" class="bmd-label-floating">Alamat</label>
                      <input disabled  type="text" class="form-control"  name="alamat"  value="<?php echo $order->alamat; ?>">
                    </div>
                    <div class="form-group">
                      <label for="examplePass" class="bmd-label-floating">Tanggal order</label>
                      <input disabled type="text" class="form-control"  name="tanggal_order"  value="<?php echo $order->tanggal_order; ?>">
                    </div>
                    
                    <div>  <label class="bmd-label-floating">Detail Order</label>
                            <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Produk</th>
                                <th>Jumlah</th>
                                <th>Total Harga</th>
                            </tr>
                        </thead>
                       
                        <tbody>
                           <?php $total_harga=0; $no=1;  foreach ($produk as $i) {
                           ?>
                            <tr>
                                <td><?php echo $no++;     ?></td>
                                <td><?php echo $i->nama;     ?></td>
                                <td><?php echo $i->jumlah;     ?></td>
                                <td>Rp.<?php echo number_format($i->harga*$i->jumlah,0,',','.');     ?></td>
                               
                            </tr>
                           <?php
                             $total_harga += ($i->harga*$i->jumlah);                                      
                            } ?>
                        </tbody>
                         <tfoot>
                            <tr>
                                <th colspan="3" class="text-right">Total</th>
                               <th>Rp.<?php echo number_format($total_harga,0,',','.'); ?></th>
                            </tr>        
                        </tfoot>
                    </table>

                    </div>
                    <?php if($order->status==1){
                        $status='Diproses';
                    }else if($order->status==2){
                        $status='Dikirim';
                    }else if($order->status==3){
                        $status='Sukses';
                    }else{
                        $status='Dibatalkan';
                    } ?>
                    
                     <div class="form-group">
                      <label for="exampleEmail" class="bmd-label-floating">Status</label>
                      <select required class="selectpicker" name="status" data-size="7" data-style="btn btn-primary btn-round" title="<?php echo $status;?>">
                                
                            <option selected value="<?php echo $order->status;?>"><?php echo $status;?></option>
                            <option value="0">Batal</option>
                            <option value="1">Diproses</option>
                            <option value="2">Dikirim</option>
                            <option value="3">Sukses</option>
                            
                          </select>
                    </div>
                    <div class="card-footer ">
                   <input type="hidden" name="id_order" value="<?php echo $order->id_order; ?>">
                  <button type="submit" class="btn btn-fill btn-rose">Simpan</button>
                        <a href="<?php echo base_url().'admin/transaksi' ?>" class="btn btn-fill btn-warning">Batal</a>
                </div>
                  </form>
                </div>
                
              </div>
              </div></div></div>
          <!-- end row -->
        </div>
      </div>   
<!---->
<!---->
<!--include footer -->
<?php $this->load->view('admin/footer'); ?>   
<!--/footer-->
                 
        </div>
     </div>
              
              
            
              <!--   Core JS Files   -->
              <script src="<?php echo base_url() ?>admin_assets/js/core/jquery.min.js"></script>
              <script src="<?php echo base_url() ?>admin_assets/js/core/popper.min.js"></script>
              <script src="<?php echo base_url() ?>admin_assets/js/core/bootstrap-material-design.min.js"></script>
              <script src="<?php echo base_url() ?>admin_assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
              <!-- Plugin for the momentJs  -->
              <script src="<?php echo base_url() ?>admin_assets/js/plugins/moment.min.js"></script>
              <!--  Plugin for Sweet Alert -->
              <script src="<?php echo base_url() ?>admin_assets/js/plugins/sweetalert2.js"></script>
              <!-- Forms Validations Plugin -->
              <script src="<?php echo base_url() ?>admin_assets/js/plugins/jquery.validate.min.js"></script>
              <!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
              <script src="<?php echo base_url() ?>admin_assets/js/plugins/jquery.bootstrap-wizard.js"></script>
              <!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
              <script src="<?php echo base_url() ?>admin_assets/js/plugins/bootstrap-selectpicker.js"></script>
              <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
              <script src="<?php echo base_url() ?>admin_assets/js/plugins/bootstrap-datetimepicker.min.js"></script>
              <!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
              <script src="<?php echo base_url() ?>admin_assets/js/plugins/jquery.dataTables.min.js"></script>
              
              <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
              <script src="<?php echo base_url() ?>admin_assets/js/plugins/bootstrap-tagsinput.js"></script>
              <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
              <script src="<?php echo base_url() ?>admin_assets/js/plugins/jasny-bootstrap.min.js"></script>
              <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
              <script src="<?php echo base_url() ?>admin_assets/js/plugins/fullcalendar.min.js"></script>
              <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
              <script src="<?php echo base_url() ?>admin_assets/js/plugins/jquery-jvectormap.js"></script>
              <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
              <script src="<?php echo base_url() ?>admin_assets/js/plugins/nouislider.min.js"></script>
              <!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
              <script src="<?php echo base_url() ?>admin_assets/cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
              <!-- Library for adding dinamically elements -->
              <script src="<?php echo base_url() ?>admin_assets/js/plugins/arrive.min.js"></script>
              <!--  Google Maps Plugin    -->
              <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2Yno10-YTnLjjn_Vtk0V8cdcY5lC4plU"></script>
              <!-- Place this tag in your head or just before your close body tag. -->
              <script async defer src="<?php echo base_url() ?>admin_assets/buttons.github.io/buttons.js"></script>
              <!-- Chartist JS -->
              <script src="<?php echo base_url() ?>admin_assets/js/plugins/chartist.min.js"></script>
              <!--  Notifications Plugin    -->
              <script src="<?php echo base_url() ?>admin_assets/js/plugins/bootstrap-notify.js"></script>
              <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
              <script src="<?php echo base_url() ?>admin_assets/js/material-dashboard.minf066.js?v=2.1.0" type="text/javascript"></script>
              <!-- Material Dashboard DEMO methods, don't include it in your project! -->
              
              <!-- Sharrre libray -->
              <script src="<?php echo base_url() ?>admin_assets/demo/jquery.sharrre.js"></script>
              
              
            <script>/*print with js*/
                function printData() {
                    var divToPrint = document.getElementById("datatables");
                    newWin = window.open("");
                    newWin.document.write(divToPrint.outerHTML);
                    newWin.print();
                    newWin.close();
                }

                $('#btn-print').on('click', function() {
                    printData();
                })
            </script>
            <script>/*data table*/
    $(document).ready(function() {
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Cari Data transaksi",
        }
      });

      var table = $('#datatable').DataTable();

      // Edit record
      table.on('click', '.edit', function() {
        $tr = $(this).closest('tr');
        var data = table.row($tr).data();
        alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      });

      // Delete a record
      table.on('click', '.remove', function(e) {
        $tr = $(this).closest('tr');
        table.row($tr).remove().draw();
        e.preventDefault();
      });

      //Like record
      table.on('click', '.like', function() {
        alert('You clicked on Like button');
      });
    });
  </script>
             
</body>


</html>