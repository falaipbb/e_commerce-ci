 <div class="sidebar" data-color="rose" data-background-color="black" data-image="<?php echo base_url() ?>admin_assets/img/sidebar-1.jpg">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        <a href="#" class="simple-text logo-mini">
          A
        </a>
        <a href="#" class="simple-text logo-normal">
         Admin Panel
        </a>
      </div>
      <div class="sidebar-wrapper">
        <div class="user">
          <div class="photo">
            <img src="<?php echo base_url() ?>admin_assets/img/faces/user.png" />
          </div>
          <div class="user-info">
            <a data-toggle="collapse" href="#collapseExample" class="username">
              <span>
                Admin
              </span>
            </a>
            
          </div>
        </div>
        <ul class="nav">
          <li class="nav-item  ">
            <a class="nav-link" href="<?php echo base_url().'admin' ?>">
              <i class="material-icons">dashboard</i>
              <p> Dashboard </p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" data-toggle="collapse" href="#pagesExamples">
              <i class="material-icons">person</i>
              <p> User
                <b class="caret"></b>
              </p>
            </a>
            <div class="collapse" id="pagesExamples">
              <ul class="nav">
                <li class="nav-item ">
                  <a class="nav-link" href="<?php echo base_url().'admin/user_admin' ?>">
                    <span class="sidebar-mini"> A </span>
                    <span class="sidebar-normal"> Admin </span>
                  </a>
                </li>
                <li class="nav-item ">
                  <a class="nav-link" href="<?php echo base_url().'admin/user_member' ?>">
                    <span class="sidebar-mini"> M </span>
                    <span class="sidebar-normal"> Member </span>
                  </a>
                </li>        
                
              </ul>
            </div>
          </li>
          <li class="nav-item  ">
            <a class="nav-link" href="<?php echo base_url().'admin/produk'; ?>">
              <i class="material-icons">work</i>
              <p> Produk </p>
            </a>
          </li> 
           <li class="nav-item  ">
            <a class="nav-link" href="<?php echo base_url().'admin/kategori'; ?>">
              <i class="material-icons">label</i>
              <p> Kategori </p>
            </a>
          </li>
           <li class="nav-item  ">
            <a class="nav-link" href="<?php echo base_url().'admin/transaksi'; ?>">
              <i class="material-icons">credit_card</i>
              <p> Transaksi </p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" data-toggle="collapse" href="#laporan">
              <i class="material-icons">person</i>
              <p> Laporan
                <b class="caret"></b>
              </p>
            </a>
            <div class="collapse" id="laporan">
              <ul class="nav">
                <li class="nav-item ">
                  <a class="nav-link" href="<?php echo base_url().'admin/laporan_user' ?>">
                    <span class="sidebar-mini"> U </span>
                    <span class="sidebar-normal"> User </span>
                  </a>
                </li>
                <li class="nav-item ">
                  <a class="nav-link" href="<?php echo base_url().'admin/laporan_produk' ?>">
                    <span class="sidebar-mini"> P </span>
                    <span class="sidebar-normal"> Produk </span>
                  </a>
                </li> 
                 <li class="nav-item ">
                  <a class="nav-link" href="<?php echo base_url().'admin/laporan_transaksi' ?>">
                    <span class="sidebar-mini"> T </span>
                    <span class="sidebar-normal"> Transaksi </span>
                  </a>
                </li>        
                
              </ul>
            </div>
          </li>
         
          
        </ul>
      </div>
    </div>