<style type="text/css">
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 106%;
        position: absolute;
        top: 80px;
        left: -30px;
    }

    td,
    th {
        border: 1px solid #dddddd;
        text-align: center;
        padding: 4px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }

</style>



</style>
<div class="wrapper">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1 class="page-header">
                Laporan Data Produk
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-6">

                    <div class="box">
                        <div class="box-body">
                            <table>

                                <tr>
                                    <th>Gambar</th>
                                    <th>Nama</th>
                                    <th>Harga</th>
                                    <th>Stok</th>
                                    <th>Detail</th>
                                </tr>
                                <?php foreach ($produk as $data){?>
                                <tr>
                                    <td><img src="<?php echo base_url().'assets_frontend/img/products/'.$data->gambar; ?>" class="img-circle" width="100" height="100">
                                    </td>
                                    <td>
                                        <?php echo $data->nama;?>
                                    </td>
                                    <td>Rp.
                                        <?php echo $data->harga;?>
                                    </td>
                                    <td>
                                        <?php echo $data->stok;?>
                                    </td>
                                    <td>
                                        <?php echo $data->detail;?>
                                    </td>
                                </tr>
                                <?php  }?>

                            </table>


                        </div>
                    </div>

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
    </div>
</div>
