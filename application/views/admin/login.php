<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url() ?>admin_assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="<?php echo base_url() ?>admin_assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
  <?php echo $tittle;?>
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="<?php echo base_url() ?>admin_assets/maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>admin_assets/maxcdn.bootstrapcdn.com/material.css">
  <!-- CSS Files -->
  <link href="<?php echo base_url() ?>admin_assets/css/material-dashboard.minf066.css?v=2.1.0" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="<?php echo base_url() ?>admin_assets/demo/demo.css" rel="stylesheet" />
  
</head>

<body class="off-canvas-sidebar">
  
  <!-- Navbar -->
  <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top text-white">
    <div class="container">
      <div class="navbar-wrapper">
        <a class="navbar-brand" href="#"><?php echo $tittle;?></a>
      </div>
      <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
        <span class="sr-only">Toggle navigation</span>
        <span class="navbar-toggler-icon icon-bar"></span>
        <span class="navbar-toggler-icon icon-bar"></span>
        <span class="navbar-toggler-icon icon-bar"></span>
      </button>
      
    </div>
  </nav>
  
  <!-- End Navbar -->
  <div class="wrapper wrapper-full-page">
    <div class="page-header login-page header-filter" filter-color="black" style="background-image: url('<?php echo base_url() ?>admin_assets/img/login.jpg'); background-size: cover; background-position: top center;">
      <!--   data-color="blue | purple | green | orange | red | rose " -->
      <div class="container">
      <?php if($this->session->flashdata('alert')=='gagal'){ ?>
      <center> 
                   <div class="alert alert-danger alert-message" style="max-width:400px; ">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <i class="material-icons">close</i>
                    </button>
                    <span>
                      <b> GAGAL - </b> Email atau Password Salah</span>
                  </div>
      </center>
      <?php } ?>
      
       <?php if($this->session->flashdata('alert')=='belum login'){ ?>
      <center> 
                   <div class="alert alert-primary alert-message" style="max-width:400px; ">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <i class="material-icons">close</i>
                    </button>
                    <span>
                      <b> Gagal - </b> Silahkan login terlebih dahulu :D</span>
                  </div>
      </center>
      <?php } ?>
       
        <div class="row">
          <div class="col-lg-4 col-md-6 col-sm-8 ml-auto mr-auto">
            <form class="form" method="post" action="<?php echo base_url()?>home/act_login_admin">
              <div class="card card-login card-hidden">
                <div class="card-header card-header-rose text-center">
                  <h4 class="card-title">Login</h4>
                  <div class="social-line">
                    
                  </div>
                </div>
                
                <div class="card-body ">
                  <p class="card-description text-center">Silahkan Login</p>
                  
                  <span class="bmd-form-group">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="material-icons">email</i>
                        </span>
                      </div>
                      <input type="email" class="form-control" name="admin_email" placeholder="Email...">
                    </div>
                  </span>
                  <span class="bmd-form-group">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="material-icons">lock_outline</i>
                        </span>
                      </div>
                      <input type="password" name="admin_password" class="form-control" placeholder="Password...">
                    </div>
                  </span>
                </div>
                <div class="card-footer justify-content-center">
                 <button class="btn btn-rose btn-link btn-lg">Masuk</button>
                 
                </div>
              </div>
            </form>
          </div>
        </div>
        
      </div>
      
      
      <footer class="footer">
        <div class="container">
          
          <div class="copyright float-right">
            &copy;
            <script>
              document.write(new Date().getFullYear())
            </script>, made with <i class="material-icons">favorite</i> by
            Faiz.
          </div>
        </div>
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  <script src="<?php echo base_url() ?>admin_assets/js/core/jquery.min.js"></script>
  <script src="<?php echo base_url() ?>admin_assets/js/core/popper.min.js"></script>
  <script src="<?php echo base_url() ?>admin_assets/js/core/bootstrap-material-design.min.js"></script>
  <script src="<?php echo base_url() ?>admin_assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2Yno10-YTnLjjn_Vtk0V8cdcY5lC4plU"></script>
  <!-- Place this tag in your head or just before your close body tag. -->
  <script async defer src="../../../../buttons.github.io/buttons.js"></script>
  <!-- Chartist JS -->
  <script src="<?php echo base_url() ?>admin_assets/js/plugins/chartist.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="<?php echo base_url() ?>admin_assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="<?php echo base_url() ?>admin_assets/js/material-dashboard.minf066.js?v=2.1.0" type="text/javascript"></script>
  <!-- Material Dashboard DEMO methods, don't include it in your project! -->
  <script src="<?php echo base_url() ?>admin_assets/demo/demo.js"></script>
 <!-- notifikasi-->
  <script src="<?php echo base_url() ?>admin_assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Sharrre libray -->
  <script src="<?php echo base_url() ?>admin_assets/demo/jquery.sharrre.js"></script>
 
<!--custom       -->
 <script type="text/javascript">
        $('.alert-message').alert().delay(3000).slideUp('slow');
     
    // Create the defaults once
   
    </script>
  
  <script>
    $(document).ready(function() {
      md.checkFullPageBackgroundImage();
      setTimeout(function() {
        // after 1000 ms we add the class animated to the login/register card
        $('.card').removeClass('card-hidden');
      }, 700);
    });
  </script>
</body>

</html>