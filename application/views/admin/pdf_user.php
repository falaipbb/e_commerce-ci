<style type="text/css">
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 106%;
        position: absolute;
        top: 80px;
        left: -30px;
    }

    td,
    th {
        border: 1px solid #dddddd;
        text-align: center;
        padding: 4px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }

</style>



</style>
<div class="wrapper">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1 class="page-header">
                Laporan Data User
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-6">

                    <div class="box">
                        <div class="box-body">
                            <table>

                                <tr>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Alamat</th>
                                    <th>No telp</th>
                                </tr>
                                <?php foreach ($user as $data){?>
                                <tr>
                                    <td>
                                        <?php echo $data->nama;?>
                                    </td>
                                    <td>
                                        <?php echo $data->email;?>
                                    </td>
                                    <td>
                                        <?php echo $data->alamat;?>
                                    </td>
                                    <td>
                                        <?php echo $data->no_telp;?>
                                    </td>

                                </tr>
                                <?php } ?>

                            </table>


                        </div>
                    </div>

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
    </div>
</div>
