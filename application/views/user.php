<!doctype html>
<html class="no-js" lang="zxx">

<!--include header-->
<?php $this->load->view('header'); ?>
<style>
    .btn-submit {
    background: #333;
    border: medium none;
    color: #fff;
    font-size: 18px;
    font-weight: 500;
    height: 50px;
    margin: 20px 0 0;
    padding: 0;
    text-transform: capitalize;
    -webkit-transition: all 0.3s ease 0s;
    transition: all 0.3s ease 0s;
    width: 100%;
        cursor: pointer;}
    
    .btn-submit:hover{
         background: #565656;
         color: #ffffff;
    }
    .btn-info{
            display: inline-block;
    font-size: 12px;
    letter-spacing: 1px;
    margin: 0 6px 12px 6px;
    padding: 7px 16px;
    background-color: #333;
    color: #fff;
    border: 1px solid #ddd;
    text-transform: capitalize;
    }
    
    </style>

<body>
    <!-- Wrapper Start -->
    <div class="wrapper kiosk-home">
        <!-- Header Area Start -->
        <?php $this->load->view('nav'); ?>
         <?php $this->load->view('bg'); ?>
        <div class="kiosk-checkout-area ptb-70">
            <div class="container">

                <div class="row">
                    <div class="col-lg-7 col-md-7">
                        <div class="table-content table-responsive">
                            <table>
                                <thead>
                                    <tr>
                                        <th>Id Order</th>
                                        <th>Tanggal Order</th>
                                        <th>Status</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                       
                                        
                                        foreach ($item as $i) { 
                                       if($i->status==1){
                                           $status='Diproses';
                                       }else if($i->status==2){
                                           $status='Dikirim';
                                       }else if($i->status==3){
                                         $status='Sukses'; 
                                       }else{
                                          $status='Dibatalkan'; 
                                       }
                                            
                                        ?>
                                    <tr>

                                        <td class="product-name"><span class="amount">#
                                                <?php echo $i->id_order;?></span></td>
                                        <td class="product-price"><span class="amount">
                                                <?php echo $i->tanggal_order;?></span></td>
                                        <td class="product-quantity">
                                            <span class="amount"><b>
                                                    <?php echo $status;?></b></span>
                                        </td>
                                        <td><a href="<?php echo base_url().'user/detail/'; echo $i->id_order; ?>" class="btn-info">Detail</a></td>

                                        <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-5">
                        <div class="kiosk-checkbox-form">
                            <h3>Hi,
                                <?php echo $user->nama; ?>
                            </h3>
                        </div>
                        <div class="kiosk-order">
                            <h4 class="text-center">Your Detail</h4>
                            <div class="kiosk-order-table table-responsive">
                                <table>
                                    <tbody>
                                        <tr class="cart_item">
                                            <td class="product-name">
                                                Email :
                                            </td>
                                            <td class="product-total">
                                                <span class="amount">
                                                    <?php echo $user->email; ?></span>
                                            </td>
                                        </tr>
                                        <tr class="cart_item">
                                            <td class="product-name">
                                                No telp
                                            </td>
                                            <td class="product-total">
                                                <span class="amount">
                                                    <?php echo $user->no_telp; ?>
                                                </span>
                                            </td>
                                        </tr>

                                        <tr class="cart_item text-center">
                                            <td colspan="2" class="product-name">
                                                <b>Alamat</b>
                                            </td>

                                        </tr>
                                        <tr class="cart_item">

                                            <td colspan="2" class="text-center ">
                                                <span class="">
                                                    <?php echo $user->alamat; ?>
                                                </span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="kiosk-payment">
                                <div class="payment-accordion">
                                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingTwo">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                        Klik Disini Untuk Edit Profile
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                <div class="panel-body">
                                                    <form class="form" action="<?php echo base_url().'user/edit/';echo$user->id_user; ?>" method="POST" enctype="multipart/form-data" role="form">
                                                        <div class="form-group">
                                                            <label for="formGroupExampleInput">Nama</label>
                                                            <input type="text" class="form-control" id="formGroupExampleInput" name="nama" value="<?php echo $user->nama; ?>">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="formGroupExampleInput1">Email</label>
                                                            <input type="email" class="form-control" id="formGroupExampleInput2" name="email" value="<?php echo $user->email ?>">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="formGroupExampleInput2">Alamat</label>
                                                            
                                                            <textarea  class="form-control" name="alamat" id="" cols="30" rows="10"><?php echo $user->alamat ?></textarea>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="formGroupExampleInput2">No_telp</label>
                                                            <input type="number" class="form-control" id="formGroupExampleInput2" name="no_telp" value="<?php echo $user->no_telp ?>">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="formGroupExampleInput2">Password</label>
                                                            <input type="password" class="form-control" id="formGroupExampleInput2" placeholder="*Isi untuk merubah password"  name="password" >
                                                        </div>
                                                        <button class="btn-submit" type="submit">Edit</button>
                                                    </form>



                                                </div>

                                            </div>


                                        </div>

                                    </div>





                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <br>
    </div>
    <!-- Wrapper End -->


    <!--include footer    -->
    <?php $this->load->view('footer'); ?>
</body>


</html>