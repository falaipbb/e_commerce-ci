
<!doctype html>
<html class="no-js" lang="zxx">

<!--include header-->
<?php $this->load->view('header'); ?>

<body>
   
    <!-- Wrapper Start -->
     <div class="wrapper kiosk-home">
        <!-- Header Area Start -->
     <?php $this->load->view('nav'); ?>
        <!-- Header Area End -->
        <!-- Slider Area Start -->
        <div class="slider-area kiosk-slider home2-slider">
            <div class="slider-wrapper theme-default">
                <!-- Slider Background  Image Start-->
                <div id="slider" class="nivoSlider">
                    <a href="<?php echo base_url('Produk/katalog');?>"> <img src="<?php echo base_url(); ?>assets_frontend/img/slider/6.jpg" data-thumb="<?php echo base_url(); ?>assets_frontend/img/slider/6.jpg" alt="" title="#slider-1-caption1"/></a>
                    <a href="<?php echo base_url('Produk/katalog');?>"><img src="<?php echo base_url(); ?>assets_frontend/img/slider/4.jpg" data-thumb="<?php echo base_url(); ?>assets_frontend/img/slider/4.jpg" alt="" title="#slider-1-caption2"/></a>
                </div>
                <!-- Slider Background  Image Start-->
                <div id="slider-1-caption1" class="nivo-html-caption nivo-caption">
                    <div class="text-content-wrapper">
                        <div class="container">
                            <div class="text-content">
                                <h4 class="title2 wow fadeInUp mb-16" data-wow-duration="2s" data-wow-delay="0s">Sale off 10%</h4>
                                <h1 class="title1 wow fadeInUp mb-16" data-wow-duration="2s" data-wow-delay="1s">Macbook Pro 2019</h1>
                                <div class="banner-readmore wow fadeInUp mt-35" data-wow-duration="2s" data-wow-delay="2s">
                                    <a class="button slider-btn" href="<?php echo base_url();?>Produk/katalog">Shop Now</a>                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>   <?php $this->load->view('bg'); ?>
                <div id="slider-1-caption2" class="nivo-html-caption nivo-caption">
                    <div class="text-content-wrapper">
                        <div class="container">
                            <div class="text-content slide-2">
                                <h4 class="title2 wow fadeInUp mb-16" data-wow-duration="1s" data-wow-delay="1s">Sale off 50%</h4>
                                <h1 class="title1 wow fadeInUp mb-16" data-wow-duration="1s" data-wow-delay="2s">New Collection 2019</h1>
                                <div class="banner-readmore wow fadeInUp mt-35" data-wow-duration="1s" data-wow-delay="3s">
                                    <a class="button slider-btn" href="shop.html">Shop Now</a>                    
                                </div>
                            </div> 
                        </div>      
                    </div>
                </div>
            </div>
        </div>
        <!-- Slider Area End --> 
        <!-- Service Start -->
        <div class="kiosk-services ptb-70">
            <div class="container">
                <div class="row">
                    <!-- Single Service Start -->
                    <div class="col-lg-3 col-sm-6">
                        <div class="single-service">
                            <div class="service-content">
                                <i class="fa fa-bus"></i>
                                <h3>Free Shipping</h3>
                                <p>Promo Gratis Ongkir untuk semua produk dan kategori .</p>
                            </div>
                        </div>
                    </div>
                    <!-- Single Service End -->
                    <!-- Single Service Start -->
                    <div class="col-lg-3 col-sm-6">
                        <div class="single-service">
                            <div class="service-content">
                                <i class="fa fa-credit-card"></i>
                                <h3>MONEY BACK</h3>
                                <p> Dapatkan cashback dan promo menarik lainnya</p>
                            </div>
                        </div>
                    </div>
                    <!-- Single Service End -->
                    <!-- Single Service Start -->
                    <div class="col-lg-3 col-sm-6">
                        <div class="single-service">
                            <div class="service-content">
                                <i class="fa fa-phone"></i>
                                <h3>ONLINE SUPPORT</h3>
                                <p>Customer care kami siap melayani untuk pertanyaan dan keluhan anda</p>
                            </div>
                        </div>
                    </div>
                    <!-- Single Service End -->
                    <!-- Single Service Start -->
                    <div class="col-lg-3 col-sm-6">
                        <div class="single-service">
                              <div class="service-content">
                                <i class="fa fa-briefcase"></i>
                                <h3>Safe</h3>
                                <p>Dapatkan transaksi yang Aman dan Nyaman</p>
                            </div>
                        </div>
                    </div>
                    <!-- Single Service End -->
                </div>
            </div>
        </div>
        <!-- Service End --> 
        <!-- Product Area Start -->
        <!-- Product Area End --> 
        <!-- Product Area Start -->
        <div class="product-area ptb-70">
            <div class="container">
                <div class="row">
                    <div class="section-title col-lg-12">
                        <h2>Featured Products <i class="fa fa-shopping-cart"></i></h2>
                    </div>
                </div>
                <div class="row">
                 <?php $i=0; foreach($items as $d) { ?>
                    <!-- Single Product Start -->                    
                    <div class="col-lg-3 col-sm-6">
                        <div class="single-product" >
                        <style>
                            .first-img {
                                max-height: 270px;
                                min-height: 268px;
                                padding: 32px;
                                border: 1px #b2b2b2 solid;
                            }
                        </style>
                            <!-- Product Image Start -->
                            <div class="kiosk-product-img" style="min-height: 268px;">
                                <a href="<?php echo base_url();?>Produk/detail/<?php echo $d->id_produk;?>">
                                    <img class="first-img" src="<?php echo base_url(); ?>assets_frontend/img/products/<?php echo $d->gambar;?>" alt="single-product">
                                    <!--<img class="second-img" src="<?php echo base_url(); ?>assets_frontend/img/products/<?php echo $d->gambar;?>" alt="single-product">-->
                                </a>
                            </div>
                            <!-- Product Image End -->
                            <!-- Product Content Start -->
                            <div class="kiosk-product-content">
                                <p><span class="price">Rp. <?php echo $d->harga;?></span></p>
                                <h4><a href="#"><?php echo $d->nama;?></a></h4>
                                <div class="kiosk-product-action">
                                    <div class="kiosk-action-content">

                                           <?php if($this->session->userdata('status') == "login_user") {?>
                                                <form action="<?php echo base_url().'produk/tambah_katalog/' ?>" method="post">

                                                <input name="jumlah" class="number" id="numeric" type="hidden" min="1" value="1">
                                                <input type="hidden" name="id_produk" value="<?php echo $d->id_produk;?>">
                                                <button type="submit" data-toggle="tooltip" title="Add to Cart"><i class="fa fa-shopping-cart"></i></button>
                                                 <a href="<?php echo base_url();?>Produk/detail/<?php echo $d->id_produk;?>" data-toggle="tooltip" title="Product Details"><i class="fa fa-link"></i></a>
                                                </form>
                                            <?php }else{ ?>
                                                <a href="<?php echo base_url().'home/login' ?>"> <button type="submit" data-toggle="tooltip" title="Add to Cart"><i class="fa fa-shopping-cart"></i></button></a>
                                                
                                                 <a href="<?php echo base_url();?>Produk/detail/<?php echo $d->id_produk;?>" data-toggle="tooltip" title="Product Details"><i class="fa fa-link"></i></a>
                                                            <?php } ?>
                                            
                                           
                                        

                                    </div>
                                </div>
                            </div>
                            <!-- Product Content End -->
                        </div>
                    </div>
                
                    <!-- Single Product End -->
                    <?php $i++;  if ($i == 4) { break; } } ?>    
                </div>
            </div>
        </div>
        <!-- Parallax Area End -->
         
        <!-- New Products Start -->
        
        <!-- New Products End -->  
        <!-- purchase area -->
        <div class="purchase-area parallax-bg2" data-stellar-background-ratio="0.6">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 ptb-100 col-lg-12 text-center">
                        <h2 class="mb-40">This Weekend 30% Discount </h2>
                        <a class="button slider-btn" href="<?php echo base_url();?>produk/katalog">Purchsase Now</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Blog Page Start -->
        <!-- Blog Page End -->
        <!-- newsletter Start -->
        <div class="kiosk-newsletter-area ptb-70">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 mr-auto ml-auto">
                        <div class="newsletter text-center">
                            <div class="main-news-desc">
                                <div class="news-desc">
                                    <h3>Sign Up To Newsletter</h3>
                                </div>
                            </div> 
                            <div class="kiosk-subscribe">
                                <form action="<?php echo base_url().'home/subscribe'?>" method="post">
                                    <input class="subscribe" placeholder="Enter your email address" name="email" id="subscribe" name="email" type="text">
                                    <button type="submit" class="submit">subscribe</button>
                                </form>
                            </div>
                        </div>                            
                    </div>
                </div>          
            </div>
        </div>
        <!-- newsletter end -->
        <!-- Footer Start -->
       <?php $this->load->view('foot'); ?>
        <!-- Footer End -->
    </div>
    <!-- Wrapper End -->
    

<!--include footer    -->
<?php $this->load->view('footer'); ?>
</body>


</html>