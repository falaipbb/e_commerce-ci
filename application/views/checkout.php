<!doctype html>
<html class="no-js" lang="zxx">

<!--include header-->
<?php $this->load->view('header'); ?>
<style>
    .btn-submit {
    background: #333;
    border: medium none;
    color: #fff;
    font-size: 18px;
    font-weight: 500;
    height: 50px;
    margin: 20px 0 0;
    padding: 0;
    text-transform: capitalize;
    -webkit-transition: all 0.3s ease 0s;
    transition: all 0.3s ease 0s;
    width: 100%;
        cursor: pointer;}
    
    .btn-submit:hover{
         background: #565656;
         color: #ffffff;
    }
    
    </style>
<body>
    <!-- Wrapper Start -->
     <div class="wrapper kiosk-home">
        <!-- Header Area Start -->
     <?php $this->load->view('nav'); ?>
     <div class="kiosk-checkout-area ptb-70">
            <div class="container">
                                
                    <div class="row">
                       <div class="col-lg-6 col-md-6">
                        <div class="table-content table-responsive">
                                <table>
                                    <thead>
                                        <tr>
                                            <th class="product-name">Nama</th>
                                            <th class="product-price">Harga</th>
                                            <th class="product-quantity">Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       <?php
                                        $total_harga=0;
                                        $total_quantity=0;
                                        
                                        
                                        foreach ($item as $i) { 
//                                        $harga_item = $i->jumlah*$i->harga;
                                        ?>
                                        <tr>
                                           
                                            <td class="product-name"><a href="#"><?php echo $i->nama;?></a></td>
                                            <td class="product-price"><span class="amount"><?php echo $i->harga;?></span></td>
                                            <td class="product-quantity">
                                                <input readonly value="<?php echo $i->jumlah;?>" type="disable">
                                            </td>
                                           
                                        <?php 
                                        $total_quantity += $i->jumlah;
                                        $total_harga += ($i->harga*$i->jumlah);    
                                        }   
                                            $ongkir=0;?> 
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="kiosk-checkbox-form">
                                <h3>Your order (#<?php
                                    $id_order=uniqid();
                                     echo $id_order; ?>)</h3>
                            </div>
                            <div class="kiosk-order">
                                <div class="kiosk-order-table table-responsive">
                                    <table>
                                        <tbody>
                                            <tr class="cart_item">
                                                <td class="product-name">
                                                    Jumlah item
                                                </td>
                                                <td class="product-total">
                                                    <span class="amount"><?php echo $total_quantity; ?> </span>
                                                </td>
                                            </tr>
                                            <tr class="cart_item">
                                                <td class="product-name">
                                                    Ongkir (FREE)
                                                </td>
                                                <td class="product-total">
                                                    <span class="amount">Rp.<?php echo $ongkir; ?> </span>
                                                </td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr class="cart-subtotal">
                                                <th> Subtotal Keranjang</th>
                                                <td class="product-total">
                                                    <span class="amount">Rp.<?php echo $total_harga; ?></span>
                                                </td>
                                            </tr>
                                            <tr class="order-total">
                                                <th>Order Total</th>
                                                <td class="product-total">
                                                    <strong><span class="amount">Rp.<?php echo $total_harga+$ongkir; ?></span></strong>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <div class="kiosk-payment">
                                    <div class="payment-accordion">
                                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingOne">
                                                    <h4 class="panel-title">
                                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                    Alamat Pengiriman :
                                                    </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne" class="panel-collapse collapse  in show" role="tabpanel" aria-labelledby="headingOne">
                                                    <div class="panel-body">
                                                        <p><?php echo $user->alamat; ?>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingTwo">
                                                    <h4 class="panel-title">
                                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                    Pembayaran Melalui Bank BCA
                                                    </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                    <div class="panel-body">
                                                        <p>Silahkan Transfer ke Rek.<b>8208812</b>  Atas Nama :Kios-K</p>
                                                        <p>Dengan keterangan/berita No.Order(<b><?php echo $id_order; ?></b>)</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingThree">
                                                    <h4 class="panel-title">
                                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                    Pembayaran Melalui Bank Mandiri
                                                    </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                    <div class="panel-body">
                                                        <p>Silahkan Transfer ke Rek.<b>3777208812</b>  Atas Nama :Kios-K</p>
                                                        <p>Dengan keterangan/berita No.Order(<b><?php echo $id_order; ?></b>)</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <form class="form" action="<?php echo base_url().'user/order/';?><?php echo $id_order; ?>" method="POST" enctype="multipart/form-data" role="form">
                                        
                                          
                                           <input type="hidden" name="alamat" value="<?php echo $user->alamat; ?>">
                                            <button class="btn-submit" type="submit" >ORDER</button>
                                           
                                 
                                        </form>

                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
            </div>
        </div>
     
<br>     
    </div>
    <!-- Wrapper End -->
    

<!--include footer    -->
<?php $this->load->view('footer'); ?>
</body>


</html>