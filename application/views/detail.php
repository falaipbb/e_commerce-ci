<!doctype html>
<html class="no-js" lang="zxx">

<!--include header-->
<?php $this->load->view('header'); ?>

<body>
    <!-- Wrapper Start -->
    <div class="wrapper kiosk-home">
        <!-- Header Area Start -->
        <?php $this->load->view('nav'); ?>
        <div class="kiosk-product-details ptb-70">
            <div class="container">
                <div class="row">
                    <!-- Main Thumbnail Image Start -->
                    <div class="col-lg-5">
                        <!-- Thumbnail Large Image start -->
                        <div class="tab-content">
                            <div id="thumb1" class="tab-pane active">
                                <a data-fancybox="images" href="img/products/1.jpg"><img src="<?php echo base_url();?>assets_frontend/img/products/<?php echo $item->gambar;?>" alt="product-view"></a>
                            </div>

                        </div>
                        <!-- Thumbnail Large Image End -->

                        <!-- Thumbnail Image End -->

                        <!-- Thumbnail image end -->
                    </div>
                    <!-- Main Thumbnail Image End -->
                    <!-- Thumbnail Description Start -->
                    <div class="col-lg-7">
                        <div class="kiosk-product-description fix">

                            <h3 class="product-header head-h3">
                                <?php echo $item->nama;?>
                            </h3>
                            <div class="pro-price mb-10">
                                <p class="product-price"><span class="price"><b>Rp.
                                            <?php echo $item->harga;?></b></span></p>
                            </div>
                            <p class="pb-20 border-bottom">
                                <?php echo $item->detail;?>
                            </p>
                            <div class="pro-ref mtb-15">
                                <p><span class="in-stock">Ketersediaan: <b>
                                            <?php echo $item->stok;?></b></span><span class="sku">Stok</span></p>
                            </div>
                            <div class="pro-ref mb-15">
                                <p><span class="in-stock">Categories:</span><span class="sku"><?php echo $item->nama_kategori;?></span></p>
                            </div>


                            <form action="<?php echo base_url().'produk/tambah_katalog/' ?>" method="post">
                                <div class="box-quantity">
                                    <label for="">Quantity</label>
                                    <input name="jumlah" class="number" id="numeric" type="number" min="1" value="1">
                                    <input type="hidden" name="id_produk" value="<?php echo $item->id_produk;?>">

                                </div>
                                <div class="product-link">
                                    <ul class="list-inline">
                                        <li><button type="submit">add to cart</button></li>

                                    </ul>

                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- Thumbnail Description End -->
                </div>
                <!-- Row End -->
            </div>
            <!-- Container End -->
        </div>


    </div>
    <!-- Wrapper End -->
    <!--include footer    -->
    <?php $this->load->view('footer'); ?>
</body>


</html>