 <footer class="black-bg">
            <!-- Footer Top Start -->
            <div class="footer-top ptb-60">
                <div class="container">           
                    <div class="row">
                        <!-- Single Footer Start -->
                        <div class="col-lg-4  col-md-4 col-sm-6">
                            <div class="single-footer">
                                <h3>About us</h3>
                                <div class="footer-content">
                                    <p>E-commerce Codeigniter 12.6b.04 
                                    <div class="kiosk-footer-social">
                                        <ul class="kiosk-footer-list footer-top-social">
                                           
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Single Footer Start -->
                        <!-- Single Footer Start -->
                        <div class="col-lg-2  col-md-2 col-sm-6 footer-full">
                            <div class="single-footer">
                                <h3 class="footer-title">Quick Link</h3>
                                <div class="footer-content">
                                    <ul class="footer-list">
                                        <li><a href="<?php echo base_url(); ?>">Home</a></li>
                                        <li><a href="<?php echo base_url().'produk/katalog'; ?>">Shop page</a></li>
                                        <li><a href="<?php echo base_url().'home/login'; ?>">Login page</a></li>
                                        <li><a href="<?php echo base_url().'home/login'; ?>">Register page</a></li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- Single Footer Start -->
                        <!-- Single Footer Start -->
                        <div class="col-lg-3 col-md-3  col-sm-6 footer-full">
                            <div class="single-footer">
                                <h3 class="footer-title">My Account</h3>
                                <div class="footer-content">
                                    <ul class="footer-list">
                                        <li><a href="<?php echo base_url().'user/checkout' ?>'user' ?>">Profile</a></li>
                                        <li><a href="<?php echo base_url().'user/checkout' ?>">Checkout</a></li>
                                        <li><a href="<?php echo base_url().'home/login' ?>">Login</a></li>
                                        <li><a href="<?php echo base_url().'user/cart' ?>">Cart</a></li>
                                     </ul>
                                </div>
                            </div>
                        </div>
                        <!-- Single Footer Start -->
                        <!-- Single Footer Start -->
                        <div class="col-lg-3 col-md-3 col-sm-6 footer-full">
                            <div class="single-footer margin-sm0">
                                <h3>Contact us</h3>
                                <div class="footer-content">
                                    <div class="kiosk-contact-address">
                                        
                                        <span><i class="fa fa-rocket"></i>faizseeguns@gmail.com</span>
                                        <span><i class="fa fa-phone"></i>(+62)8975128532</span>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Single Footer Start -->
                    </div>
                    <!-- Row End -->
                </div>
                <!-- Container End -->
            </div>
            <!-- Footer Top End -->
            <!-- Footer Bottom Start -->
            <div class="footer-bottom">
                <div class="container">
                    <div class="footer-bottom-content">
                        <p class="copy-right-text">Copyright ©  <a  href="#">2019</a></p>
                        <div class="kiosk-footer-social hidden-content">
                            <ul class="kiosk-footer-list">
                                <li><a href="#"><img src="<?php echo base_url(); ?>assets_frontend/img/footer/visa.png" alt=""></a></li>
                                <li><a href="#"><img src="<?php echo base_url(); ?>assets_frontend/img/footer/amarcan.png" alt=""></a></li>
                                <li><a href="#"><img src="<?php echo base_url(); ?>assets_frontend/img/footer/discover.png" alt=""></a></li>
                                <li><a href="#"><img src="<?php echo base_url(); ?>assets_frontend/img/footer/mastercard.png" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- Container End -->
            </div>
            <!-- Footer Bottom End -->
        </footer>