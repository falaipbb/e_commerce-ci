<div class="kiosk-header-bottom header-sticky">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-3 col-lg-2 col-sm-5 col-5">
                            <div class="logo">
                                <a href="<?php echo base_url();?>home/index"><img src="<?php echo base_url();?>assets_frontend/img/logo/logo.png" alt="logo image"></a>
                            </div>
                        </div>
                        <!-- Primary Vertical-Menu End -->
                        <!-- Search Box Start -->
                        <div class="col-xl-6 col-lg-7 d-none d-lg-block text-center">
                            <div class="kiosk-middle-menu">
                                <nav>
                                    <ul class="kiosk-middle-menu-list">
                                        <li><a href="<?php echo base_url();?>Home">home</a>
                                            
                                        </li>
                                                                     
                                        <li><a href="<?php echo base_url().'Produk/katalog'; ?>">shop</a>
                                        </li>                                        
                                        
                                        
                                                                                
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <!-- Search Box End -->
                        <!-- Cartt Box Start -->
                        <div class="col-lg-3 col-sm-7 col-7">
                            <div class="kiosk-cart text-right">
                                <ul>
                                    <li><a href="#" ><i class="fa fa-shopping-cart"></i><span class="cart-counter"><?php echo $num_keranjang; ?></span></a>
                                       
                                        <ul class="kiosk-dropdown main-kiosk-cart">
                                            <li>
                                              
                                                <div class="cart-footer fix" style="margin-top:-15px;">
                                                    
                                                    <div class="kiosk-cart-button">
                                                        <a class="button slider-btn" href="<?php echo base_url();?>User/cart">View Cart</a>
                                                        <a class="button slider-btn f-right mr-0" href="<?php echo base_url();?>user/checkout">Checkout</a>
                                                    </div>
                                                </div>
                                                <!-- Cart Footer Inner End -->
                                            </li>
                                        </ul>
                                    </li>
                                   
                                  <?php if($this->session->userdata('status') == "login_user") {?>   
                                <li><a href="<?php echo base_url().'user' ?>"><i class="fas fa-user"></i></a>
                                   
                                       <ul class="kiosk-dropdown " style="width: 113px;">
                                        <li>
                                          <div class="cart-footer fix" style="margin-top:-15px;">
                                            <div class="kiosk-cart-button">
                                                <a class="button slider-btn" href="<?php echo base_url();?>User/logout">Logout</a>
                                            </div>
                                            </div>
                                            <!-- Cart Footer Inner End -->
                                        </li>
                                    </ul>
                                   </li>
                                    <?php }else{ ?>
                                    <li><a href="<?php echo base_url();?>home/login"><i class="fas fa-user"></i></a></li>
                                    <?php }?> 
                              </ul>
                            </div>
                        </div>
                        <!-- Cartt Box End -->
                        <div class="col-sm-12 d-lg-none">
                            <div class="mobile-menu">
                                <nav>
                                    <ul>
                                        <li><a href="<?php echo base_url() ?>">home</a>
                                            <!-- Home Version Dropdown Start -->
                                            <ul>
                                                <li><a href="<?php echo base_url().'home/login'; ?>">Login</a></li>
                                                <li><a href="<?php echo base_url().'produk/katalog'; ?>">Katalog</a></li>
                                            </ul>
                                            <!-- Home Version Dropdown End -->
                                        </li>
                                   
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <!-- Mobile Menu  End -->                        
                    </div>
                    <!-- Row End -->
                </div>
                <!-- Container End -->
            </div>