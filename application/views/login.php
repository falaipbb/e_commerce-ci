<!doctype html>
<html class="no-js" lang="zxx">

<!--include header-->
<?php $this->load->view('header'); ?>

<body>
    <!-- Wrapper Start -->
    <div class="wrapper kiosk-home">
        <!-- Header Area Start -->
        <?php $this->load->view('nav'); ?>
        <?php  if($this->session->flashdata()){?>
        <div class="section-title col-lg-12 mb-50 alert-message">
            <h2>
                <?php echo $this->session->flashdata('alert'); ?><i class="fa fa-warning"></i></h2>
        </div>
        <?php } ?>
        <div class="log-in ptb-70">
            <div class="container">
                <?php $this->load->view('bg'); ?>
                <div class="row">
                    <!-- New Customer Start -->
                    <div class="col-sm-6">
                        <div class="well">
                            <div class="kiosk-login">
                                <h3 class="mb-10">Log in to your account</h3>
                                <p class="mb-10"><strong>Silahkan login terlebih dahulu</strong></p>
                                <form method="post" action="<?php echo base_url().'home/act_login' ?>">
                                    <div class="form-group">
                                        <label class="control-label">Email</label>
                                        <input type="text" name="email" placeholder="Email" id="input-email" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Password</label>
                                        <input type="password" name="pass" placeholder="Password" id="input-password" class="form-control">
                                    </div>
                                    <input type="submit" value="Login" class="kiosk-login-btn">
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- New Customer End -->
                    <!-- Login Area Start -->
                    <div class="col-sm-6">
                        <div class="well">
                            <div class="kiosk-login">
                                <h3 class="mb-10 mtop">Create An Account</h3>
                                <form method="post" action="<?php echo base_url().'home/register' ?>">
                                    <fieldset>
                                        <legend>Silahkan isi Data diri</legend>
                                        <div class="form-group">
                                            <label class="control-label" for="l-name"><span class="require">*</span>Nama</label>
                                            <input type="text" class="form-control" id="l-name" name="nama">
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label" for="email"><span class="require">*</span>Email</label>
                                            <input type="email" class="form-control" id="email" name="email">
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label" for="number"><span class="require">*</span>Alamat lengkap</label>
                                            <input type="text"  placeholder="" class="form-control" id="number" name="alamat">
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label" for="number"><span class="require">*</span>No Telp</label>
                                            <input type="number" class="form-control" id="number" name="no_telp">
                                        </div>
                                    </fieldset>
                                    <fieldset>
                                        <legend>Your Password</legend>
                                        <div class="form-group">
                                            <label class="control-label" for="pwd"><span class="require">*</span>Password:</label>
                                            <input type="password" class="form-control" id="pwd" name="password">
                                        </div>

                                    </fieldset>
                                    <div class="buttons kiosk-input-area">
                                        <input type="submit" value="Submit" class="kiosk-button-field">
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                    <!-- Login Area End -->

                </div>
                <!-- Row End -->
            </div>
            <!-- Container End -->
        </div>
    </div>
    <!-- Wrapper End -->

    <!-- newsletter end -->
    <!-- Footer Start -->

    <?php $this->load->view('foot'); ?>
    <!-- Footer End -->

    <!-- Wrapper End -->

    <!--include footer    -->
    <?php $this->load->view('footer'); ?>
    <script type="text/javascript">
        $('.alert-message').alert().delay(3000).slideToggle('slow');
    </script>
</body>


</html>