
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?php echo $tittle; ?></title>
    <meta name="description" content="Default Description">
    <meta name="keywords" content="E-commerce" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- place favicon.ico in the root directory -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>assets_frontend/img/icon/favicon.png">
    <!-- Google Font css -->
    <link href="https://fonts.googleapis.com/css?family=Lily+Script+One" rel="stylesheet"> 

    <!-- mobile menu css -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets_frontend/css/meanmenu.min.css">
    <!-- animate css -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets_frontend/css/animate.css">
    <!-- nivo slider css -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets_frontend/css/nivo-slider.css">
    <!-- owl carousel css -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets_frontend/css/owl.carousel.min.css">
    <!-- slick css -->
   <link rel="stylesheet" href="<?php echo base_url();?>assets_frontend/css/slick.css">
    <!-- price slider css -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets_frontend/css/jquery-ui.min.css">
    <!-- fontawesome css -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets_frontend/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets_frontend/fontawesome/css/all.min.css">
     <!-- fancybox css -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets_frontend/css/jquery.fancybox.css">     
    <!-- bootstrap css -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets_frontend/css/bootstrap.min.css">
    <!-- default css  -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets_frontend/css/default.css">
    <!-- style css -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets_frontend/style.css">
    <!-- responsive css -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets_frontend/css/responsive.css">

    <!-- modernizr js -->
    <script src="<?php echo base_url();?>assets_frontend/js/vendor/modernizr-2.8.3.min.js"></script>
</head>
