<!doctype html>
<html class="no-js" lang="zxx">

<!--include header-->
<?php $this->load->view('header'); ?>

<body>
  <?php $this->load->view('bg'); ?>
    <!-- Wrapper Start -->
    <div class="wrapper kiosk-home">
        <!-- Header Area Start -->
        <?php $this->load->view('nav'); ?>
        <div class="main-shop-page ptb-70">
            <div class="container">
                <!-- Row End -->
                <div class="row">
                    <!-- Sidebar Shopping Option Start -->
                    <div class="col-lg-3  order-2">
                        <div class="sidebar white-bg">
                            <div class="single-sidebar category-sidebar">
                                <div class="group-title">
                                    <h2>categories</h2>
                                </div>
                                <ul>
                                    <li><a href="?by=">ALL</a></li>
                                    <?php foreach($kategori as $k){?>
                                    <li><a href="<?php echo '?by='.$k->id_kategori ?>">
                                            <?php echo $k->nama_kategori ?></a></li>
                                    <?php } ?>
                                </ul>
                            </div>

                        </div>
                    </div>
                    <!-- Sidebar Shopping Option End -->
                    <!-- Product Categorie List Start -->
                    <div class="col-lg-9 order-lg-2">
                        <!-- Grid & List View Start -->
                        <div class="grid-list-top fix mb-10">
                            <!-- Toolbar Short Area Start -->

                            <!-- Toolbar Short Area End -->
                            <div class="grid-list-view f-left">
                                <ul class="list-inline nav">
                                    <li><a data-toggle="tab" href="#grid-view"><i class="fa fa-th"></i></a></li>
                                    <li><a data-toggle="tab" href="#list-view"><i class="fa fa-list-ul"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- Grid & List View End -->
                        <div class="main-categorie product-area">
                            <!-- Grid & List Main Area End -->
                            <div class="tab-content fix">
                                <div id="grid-view" class="tab-pane active">
                                    <div class="row">
                                        <!-- Single Product Start -->
                                        <?php foreach($items as $d) { ?>
                                        <div class="col-lg-4 col-sm-6">
                                            <div class="single-product">
                                                <!-- Product Image Start -->
                                                <div class="kiosk-product-img">
                                                    <a href="<?php echo base_url();?>Produk/detail/<?php echo $d->id_produk;?>">
                                                        <img class="first-img" src="<?php echo base_url(); ?>assets_frontend/img/products/<?php echo $d->gambar;?>" alt="single-product" alt="single-product">
                                                        <img class="second-img" src="<?php echo base_url(); ?>assets_frontend/img/products/<?php echo $d->gambar;?>" alt="single-product">
                                                    </a>
                                                </div>
                                                <!-- Product Image End -->
                                                <!-- Product Content Start -->
                                                <div class="kiosk-product-content">
                                                    <p><span class="price">Rp.
                                                            <?php echo $d->harga;?></span></p>
                                                    <h4><a href="#">
                                                            <?php echo $d->nama;?></a></h4>
                                                    <div class="kiosk-product-action">
                                                        <div class="kiosk-action-content">
                                                            <?php if($this->session->userdata('status') == "login_user") {?>
                                                            <form action="<?php echo base_url().'produk/tambah_katalog/' ?>" method="post">

                                                                <input name="jumlah" class="number" id="numeric" type="hidden" min="1" value="1">
                                                                <input type="hidden" name="id_produk" value="<?php echo $d->id_produk;?>">
                                                                <button type="submit" data-toggle="tooltip" title="Add to Cart"><i class="fa fa-shopping-cart"></i></button>
                                                            </form>
                                                            <?php }else{ ?>
                                                            <a href="<?php echo base_url().'home/login' ?>"> <button type="submit" data-toggle="tooltip" title="Add to Cart"><i class="fa fa-shopping-cart"></i></button></a>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Product Content End -->
                                            </div>
                                        </div>
                                        <!-- Single Product End -->
                                        <?php } ?>

                                    </div>
                                </div>
                                <!-- #grid view End -->
                                <div id="list-view" class="tab-pane">
                                    <!-- Single Product Start -->
                                    <!-- Single Product Start -->
                                    <?php foreach($items as $d) { ?>
                                    <div class="single-product">
                                        <!-- Product Image Start -->
                                        <div class="kiosk-product-img">
                                            <a href="<?php echo base_url();?>Produk/detail/<?php echo $d->id_produk;?>">
                                                <img class="first-img" src="<?php echo base_url(); ?>assets_frontend/img/products/<?php echo $d->gambar;?>" alt="single-product">
                                                <img class="second-img" src="<?php echo base_url(); ?>assets_frontend/img/products/<?php echo $d->gambar;?>" alt="single-product">
                                            </a>
                                        </div>
                                        <!-- Product Image End -->
                                        <!-- Product Content Start -->
                                        <div class="kiosk-product-content">
                                            <h4><a href="product.html">
                                                    <?php echo $d->nama;?></a></h4>
                                            <p><span class="price">Rp.
                                                    <?php echo $d->harga;?></span></p>
                                            <p>
                                                <?php echo $d->detail;?>
                                            </p>
                                            <div class="kiosk-product-action">
                                                <div class="kiosk-action-content">
                                                    <?php if($this->session->userdata('status') == "login_user") {?>
                                                    <form action="<?php echo base_url().'produk/tambah_katalog/' ?>" method="post">

                                                        <input name="jumlah" class="number" id="numeric" type="hidden" min="1" value="1">
                                                        <input type="hidden" name="id_produk" value="<?php echo $d->id_produk;?>">
                                                        <button type="submit" data-toggle="tooltip" title="Add to Cart"><i class="fa fa-shopping-cart"></i></button>
                                                    </form>
                                                    <?php }else{ ?>
                                                    <a href="<?php echo base_url().'home/login' ?>"> <button type="submit" data-toggle="tooltip" title="Add to Cart"><i class="fa fa-shopping-cart"></i></button></a>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Product Content End -->
                                    </div>
                                    <?php } ?>
                                </div>
                                <!-- #list view End -->

                            </div>
                            <!-- Grid & List Main Area End -->
                        </div>

                    </div>
                    <!-- product Categorie List End -->
                </div>
                <!-- Row End -->
            </div>
            <!-- Container End -->
        </div>
    </div>
    <!-- Wrapper End -->


    <!--include footer    -->
    <?php $this->load->view('footer'); ?>
</body>


</html>